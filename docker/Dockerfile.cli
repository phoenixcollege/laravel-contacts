FROM public.ecr.aws/composer/composer:2 as composer
WORKDIR /app
COPY ./codebase/ /app/
RUN composer install  \
    --ignore-platform-reqs \
    --no-ansi \
    --no-autoloader \
    --no-dev \
    --no-interaction \
    --no-scripts
RUN cp -a /app/resources.copy/. /app/resources/ && \
    cp -a /app/config.copy/. /app/config/ && \
    cp -a /app/public.copy/. /app/public/
RUN composer dump-autoload --optimize --classmap-authoritative

FROM public.ecr.aws/phoenixcollege/laravel-php-cli-mssql:8.2
RUN apk add --no-cache --virtual .build-deps freetype-dev libjpeg-turbo-dev libpng-dev $PHPIZE_DEPS linux-headers && \
    apk add --no-cache freetype libjpeg-turbo libpng && \
    docker-php-ext-configure gd --enable-gd --with-freetype --with-jpeg && \
    docker-php-ext-install -j$(nproc) gd && \
    apk del .build-deps
COPY ./codebase/ /app/
COPY ./docker/scripts/on_create.sh /on_create.sh
COPY ./docker/scripts/on_update.sh /on_update.sh
RUN cp -a /app/config.copy/. /app/config/ && \
    mkdir -p /app/resources/views && \
    cp -a /app/resources.copy/views/. /app/resources/views/ && \
    rm -rf /app/public /app/public.copy /app/resources.copy /app/config.copy && \
    chmod +x /on_create.sh && \
    chmod +x /on_update.sh
COPY --from=composer /app/vendor/ /app/vendor/
COPY --from=composer /app/public.copy/index.php /app/public/index.php
RUN chown -R root:www-data /app && \
    chmod -R 0750 /app && \
    chown -R www-data:www-data /app/storage && \
    chown -R www-data:www-data /app/bootstrap/cache
VOLUME /app
VOLUME /tmp

#ECS Exec compatibility
RUN mkdir -p /var/lib/amazon
RUN chmod 777 /var/lib/amazon
VOLUME /var/lib/amazon

RUN mkdir -p /var/log/amazon
RUN chmod 777 /var/log/amazon
VOLUME /var/log/amazon
