#!/usr/bin/env sh
cd /app
/usr/local/bin/php artisan migrate --seed --force
if [ -n "$ADMIN_USER_ID" ]; then
  /usr/local/bin/php artisan role:set "$ADMIN_USER_ID"
fi
