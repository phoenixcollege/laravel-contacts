#!/usr/bin/env sh
cd /app
/usr/local/bin/php artisan view:cache
/usr/local/bin/php artisan route:cache
/usr/local/bin/php artisan config:cache
php-fpm
