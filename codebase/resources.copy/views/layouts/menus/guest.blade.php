@guest
    @php $menus = \Smorken\Menu\Facades\Menu::getMenusByKey('guest'); @endphp
    @include('layouts.menus._menu_items')
@endguest
