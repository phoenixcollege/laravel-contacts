@if ($menus)
    @php
        $submenu = null;
    @endphp
    @foreach ($menus as $menu)
        @php
            $active = \Smorken\Menu\Facades\Menu::isActiveChain($controller ?? null, $menu);
            $submenu = (is_null($submenu) && $active && count($menu->children)) ? $menu->children : $submenu;
        @endphp
        @include('layouts.menus._menu_item')
    @endforeach
    @if ($submenu)
        @php \Smorken\Menu\Facades\Menu::setSubMenus($submenu); @endphp
    @endif
@endif
