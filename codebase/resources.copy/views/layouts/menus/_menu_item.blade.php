@if ($menu->visible)
    <li class="nav-item">
        <a class="nav-link {{ $active ? 'active' : null }}"
           aria-current="{{ $active ? 'true' : 'false' }}"
           href="{{ action($menu->action) }}">{{ $menu->name }}</a>
    </li>
@endif
