@php $value = (array) ($value ?? []); @endphp
<div class="clear-alert text-bg-{{ $key }} p-1" role="alert">
    <div class="container">
        @foreach ($value as $v)
            <div class="mb-1">{{ $v }}</div>
        @endforeach
    </div>
</div>
