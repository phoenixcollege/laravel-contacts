<div id="modal" class="modal fade" role="dialog" aria-labelledby="modal-title" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modal-title">
                    @isset($title)
                        {{ $title }}
                    @endisset
                </h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="modal-body">
                <p>
                    @isset($body)
                        {!! $body !!}
                    @endisset
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-block w-100" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
