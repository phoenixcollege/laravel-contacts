<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Ip $model
 * @var string $client_ip
 * @var \Illuminate\Support\Collection $locations
 * @var \App\Contracts\Models\Location $location
 */
?>
@extends($viewLayout ?? 'layouts.app')
@section('content')
    <h4>IPs Authorized</h4>
    @if (count($locations))
        <div class="card mb-2">
            <div class="card-body">
                <form method="post">
                    @csrf
                    <div class="row">
                        <div class="col">
                            <div class="form-control-plaintext">{{ $client_ip }}</div>
                        </div>
                        <div class="col">
                            <label for="location_id" class="visually-hidden">Location</label>
                            <select class="form-select" name="location_id">
                                @foreach ($locations as $location)
                                    <option value="{{ $location->id }}">{{ $location->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <label for="valid_until" class="visually-hidden">Valid for</label>
                            <select name="valid_until" class="form-select">
                                @foreach ($times as $value => $text)
                                    <option value="{{ $value }}" {{ $value === $default_time ? 'selected' : ''}}>{{ $text }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-success">Authorize</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if ($models && count($models))
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>IP Address</th>
                    <th>Location</th>
                    <th>Valid Until</th>
                    <th>Updated By</th>
                    <th>Last Updated</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($models as $model)
                    <tr>
                        <td>{{ $model->id }}</td>
                        <td>{{ $model->ip }}</td>
                        <td>{{ $model->location ? $model->location->name : $model->location_id }}</td>
                        <td>
                            {{ $model->valid_until }}
                            <small>({{ $model->valid_until->diffForHumans() }})</small>
                        </td>
                        <td>{{ $model->user_id }}</td>
                        <td>{{ $model->updated_at }}</td>
                        <td>
                            <a href="{{ action([$controller, 'delete'], ['id' => $model->id]) }}"
                               title="Delete {{ $model->id }}">
                                Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $models->links() }}
        @else
            <div class="text-muted">No records found.</div>
        @endif
    @else
        <div class="text-danger">No "locations" have been setup for your account. Please contact your administrator.
        </div>
    @endif
@endsection
