<a href="{{ action([$controller, 'update'], $params) }}" class="text-primary mr-5 me-5"
   title="Update {{ $model->getKey() }}">{{ $text ?? 'update' }}</a>
