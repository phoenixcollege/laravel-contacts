@if ($attr_value !== false)
    @if (is_null($attr_value))
        {{ $attr_key }}
    @elseif ($attr_value === true)
        {{ $attr_key }}="{{ $attr_key }}"
    @else
        {{ $attr_key }}="{{ $attr_value }}"
    @endif
@endif
