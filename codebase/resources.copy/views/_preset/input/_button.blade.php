@php
    $type = (!isset($type) || !is_string($type) ? 'submit' : $type);
    $name = $name ?? 'button-'.$type.'-'.rand(0, 1000);
@endphp
<button @if (isset($id)) id="{{ $id }}" @endif
        @include('_preset.input.__attrs', ['attrs' => ['type' => $type, 'name' => $name ?? null, 'class' => 'btn ' . $classes ?? '']])
        @include('_preset.input.__attrs', ['attrs' => $add_attrs ?? []])
>
    {{ $title ?? 'Go' }}
</button>
