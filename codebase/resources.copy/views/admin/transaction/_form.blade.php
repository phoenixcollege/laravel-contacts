<?php
$categories = \App\View\Helpers\CategoryHelper::toArray($categories);
$images = \App\View\Helpers\ImageHelper::toArray($images);
?>
@include('_preset.input.g_input', ['name' => 'transaction', 'title' => 'Transaction Name'])
@include('_preset.input.g_select', [
    'name' => 'category_id',
    'title' => 'Category',
    'items' => $categories,
])
@include('_preset.input.g_select', [
    'name' => 'image_id',
    'title' => 'Image',
    'items' => ['0' => 'None'] + $images,
])
@include('_preset.input.g_check_bool', ['name' => 'active', 'title' => 'Active'])
