@extends('layouts.app')
@include('_preset.controller.view', [
    'title' => 'Transaction Administration',
    'extra' => [
        'Category' => $model->category ? \App\View\Helpers\CategoryHelper::toStringWithLocation($model->category) : 'Unknown',
        'Image Detail' => $model->image ? \App\View\Helpers\ImageHelper::toString($model->image) : 'None',
    ]
])
@section('content')
    @if ($model->image_id)
        <div class="my-2">
            <img src="{{ route('view.image', ['id' => $model->image_id]) }}"
                 class="img-thumbnail" title="Image for {{ $model->id }}">
        </div>
    @endif
@append
