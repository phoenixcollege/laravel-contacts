<?php
/**
 * @var \Illuminate\Support\Collection $models
 */
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Transaction Administration'])
    @includeIf('admin.transaction._filter_form')
    @include('_preset.controller.index_actions._create')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Transaction</th>
                <th>Category</th>
                <th>Image Detail</th>
                <th>Status</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php
                $params = array_merge([$model->getKeyName() => $model->getKey()],
                    $filter->except(['page']));
                $imageText = $model->image ? \App\View\Helpers\ImageHelper::toString($model->image) : $model->image_id;
                $categoryText = $model->category ? \App\View\Helpers\CategoryHelper::toStringWithLocation($model->category) : $model->category_id;
                ?>
                <tr>
                    <td>@include('_preset.controller.index_actions._view', ['value' => $model->getKey()])</td>
                    <td>{{ $model->transaction }}</td>
                    <td>{{ $categoryText }}</td>
                    <td>{{ $imageText }}</td>
                    <td>{{ $model->active_string }}</td>
                    <td>
                        @include('_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends($filter->except(['page']))->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
