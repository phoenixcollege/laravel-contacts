<?php
$filtered = 'border border-success';
$categories = \App\View\Helpers\CategoryHelper::toArray($categories);
?>
<div class="card my-2">
    <div class="card-body">
        <form class="row g-3 align-items-center" method="get">
            <div class="col-md">
                @include('_preset.input._label', ['name' => 'f_categoryId', 'title' => 'Category', 'label_classes' => 'visually-hidden'])
                @include('_preset.input._select', [
                'name' => 'f_categoryId',
                'classes' => $filter->f_categoryId ? $filtered : '',
                'value' => $filter->f_categoryId,
                'items' => ['' => '-- Any Category --'] + $categories
                ])
            </div>
            <div class="col-md">
                @include('_preset.input._label', ['name' => 'f_active', 'title' => 'Active', 'label_classes' => 'visually-hidden'])
                @include('_preset.input._select', [
                'name' => 'f_active',
                'classes' => $filter->has('f_active') ? $filtered : '',
                'value' => $filter->f_active,
                'items' => ['' => '-- Any Status --'] + \App\Enums\Active::toArray()
                ])
            </div>
            <div class="col-md">
                <button type="submit" class="btn btn-primary">Filter</button>
                <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger"
                   title="Reset filter">Reset</a>
            </div>
        </form>
    </div>
</div>
