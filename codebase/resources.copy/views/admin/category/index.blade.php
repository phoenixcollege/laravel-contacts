@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Category Administration'])
    @includeIf('admin.category._filter_form')
    @include('_preset.controller.index_actions._create')

    @if (count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Category</th>
                <th>Location</th>
                <th>Status</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    $filter->toArray()); ?>
                <tr id="row-for-{{ $model->getKey() }}">
                    <td>@include('_preset.controller.index_actions._view', ['value' => $model->getKey()])</td>
                    <td>{{ $model->category }}</td>
                    <td>{{ $model->location ? $model->location->name : $model->location_id }}</td>
                    <td>{{ $model->active_string }}</td>
                    <td>
                        <a href="{{ action([\App\Http\Controllers\Admin\Transaction\Controller::class, 'index'], ['f_categoryId' => $model->id]) }}"
                           title="Edit Transactions for {{ $model->id }}">
                            transactions
                        </a>
                    </td>
                    <td>
                        @include('_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends($filter->except(['page']))->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
