<?php $filtered = 'border border-success'; ?>
<div class="card my-2">
    <div class="card-body">
        <form class="row g-3 align-items-center" method="get">
            <div class="col-md">
                @include('_preset.input._label', ['name' => 'f_locationId', 'title' => 'Location', 'label_classes' => 'visually-hidden'])
                @include('_preset.input._select', [
                'name' => 'f_locationId',
                'classes' => $filter->f_locationId ? $filtered : '',
                'value' => $filter->f_locationId,
                'items' => ['' => '-- Any Location --'] + $locations->pluck('name', 'id')->all()
                ])
            </div>
            <div class="col-md">
                @include('_preset.input._label', ['name' => 'f_active', 'title' => 'Active', 'label_classes' => 'visually-hidden'])
                @include('_preset.input._select', [
                'name' => 'f_active',
                'classes' => $filter->f_active ? $filtered : '',
                'value' => $filter->f_active,
                'items' => ['' => '-- Any Status --'] + \App\Enums\Active::toArray()
                ])
            </div>
            <div class="col-md">
                <button type="submit" class="btn btn-primary">Filter</button>
                <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger"
                   title="Reset filter">Reset</a>
            </div>
        </form>
    </div>
</div>
