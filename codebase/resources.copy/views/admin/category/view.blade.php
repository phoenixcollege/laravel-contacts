@extends('layouts.app')
@include('_preset.controller.view', [
    'title' => 'Category Administration',
    'extra' => ['Location' => $model->location ? $model->location->name : 'Unknown'],
])
