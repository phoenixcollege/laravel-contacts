@include('_preset.input.g_input', ['name' => 'category', 'title' => 'Category'])
@include('_preset.input.g_select', [
    'name' => 'location_id',
    'title' => 'Location',
    'items' => $locations->pluck('name', 'id')->all()
])
@include('_preset.input.g_check_bool', ['name' => 'active', 'title' => 'Active'])
