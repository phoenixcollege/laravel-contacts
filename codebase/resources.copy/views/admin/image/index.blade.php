<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Image $model
 */
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Image Administration'])
    @include('_preset.controller.index_actions._create')
    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Info</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php $params = array_merge([$model->getKeyName() => $model->getKey()],
                    isset($filter) ? $filter->except(['page']) : []); ?>
                <tr id="row-for-{{ $model->id }}">
                    <td>
                        <img class="img-thumbnail"
                             src="{{ route('view.image', ['id' => $model->getKey()]) }}"
                             alt="Image for {{ $model->getKey() }}">
                    </td>
                    <td>{{ $model->name }}</td>
                    <td>{{ $model->id }} - {{ $model->type }} - {{ $model->size }} bytes</td>
                    <td>
                        @include('_preset.controller.index_actions._update_delete')
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends($filter->except(['page']))->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
