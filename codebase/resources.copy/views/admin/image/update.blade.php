<?php
$params = array_merge([$model->getKeyName() => $model->getKey()], $filter->toArray());
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._to_index')
    @include('_preset.controller._title', ['title' => 'Image Administration'])
    <h5 class="mb-2">Update record [{{ $model->getKey() }}]</h5>
    <form method="post" enctype="multipart/form-data"
          action="{{ action([$controller, $action ?? 'doUpdate'], $params) }}">
        @csrf
        <input type="hidden" name="_id" value="{{ $model->getKey() }}">
        <div class="mb-4">
            @include('admin.image._form')
        </div>
        <div class="">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ action([$controller, 'index'], $params) }}" title="Cancel" class="btn btn-outline-secondary">Cancel</a>
        </div>
    </form>
    <div class="my-2">
        @if ($model->image)
            <img class="img-thumbnail" src="{{ $model->getSrcString() }}" alt="image for {{ $model->id }}"/>
        @endif
    </div>
@endsection
