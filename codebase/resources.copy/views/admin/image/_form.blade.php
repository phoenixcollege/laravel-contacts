<?php
$creating = $creating ?? false;
?>
@if($creating)
    @include('_preset.input.g_input', [
        'type' => 'file',
        'name' => 'image',
        'title' => 'Image',
        'add_attrs' => [
            'accept' => 'image/*',
        ]
    ])
@endif
@include('_preset.input.g_input', ['name' => 'name', 'title' => 'Name'])
@if($creating)
    <small class="form-text text-muted">If left blank, the file name will be used.</small>
@endif
