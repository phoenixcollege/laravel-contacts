<?php
$params = array_merge([$model->getKeyName() => $model->getKey()], $filter->toArray());
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._to_index')
    @include('_preset.controller._title', ['title' => 'Image Administration'])
    <h5 class="mb-2">Create new record</h5>
    <form method="post" enctype="multipart/form-data" action="{{ action([$controller, 'doCreate'], $params) }}">
        @csrf
        <div class="mb-4">
            @include('admin.image._form', ['creating' => true])
        </div>
        <div class="">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ action([$controller, 'index'], $params) }}" title="Cancel" class="btn btn-outline-secondary">Cancel</a>
        </div>
    </form>
@endsection
