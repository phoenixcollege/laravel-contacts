<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\User $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends('layouts.app')
@section('content')
    <h4>Access Administration</h4>
    @include('admin.access._filter_form')
    <h5 class="mt-2">Users available for assignment</h5>
    @if ($models && count($models))
        <table class="table table-striped mt-2">
            <thead>
            <tr>
                <th>ID</th>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Locations</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <tr id="row-for-{{ $model->id }}">
                    <td>
                        <a href="{{ action([$controller, 'assign'], array_replace($filter->toArray(), ['id' => $model->id])) }}"
                           title="Manage assignments for {{ $model->id }}">
                            {{ $model->id }}
                        </a>
                    </td>
                    <td>{{ $model->last_name }}</td>
                    <td>{{ $model->first_name }}</td>
                    <td>
                        @if ($model->locations && count($model->locations))
                            {{ implode(', ', $model->locations->pluck('name')->toArray()) }}
                        @else
                            --
                        @endif
                    </td>
                    <td>
                        @if ($model->locations && count($model->locations))
                            <a href="{{ action([$controller, 'remove'], array_replace($filter->toArray(), ['id' => $model->id])) }}"
                               title="Remove all assignments for {{ $model->id }}" class="text-danger">
                                remove all
                            </a>
                        @else
                            &nbsp;
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $models->appends($filter->except(['page']))->links() }}
    @else
        <div class="text-muted">No users found.</div>
    @endif
    <div class="alert alert-info">
        Users who can authorize computers must also have the Manager role.
        This can be set from <a
                href="{{ action([\Smorken\Auth\Proxy\Http\Controllers\Admin\User\Controller::class, 'index']) }}"
                title="Admin Users">Admin -> Users</a>.
    </div>
@stop
