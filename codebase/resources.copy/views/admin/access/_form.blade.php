<form method="post"
      action="{{ action([$controller, 'doAssign'], array_replace($filter->toArray(), ['id' => $user->id])) }}">
    @csrf
    @if ($locations && count($locations))
        @foreach ($locations as $location)
            <div class="form-check mb-2">
                <input type="hidden" name="location[{{ $location->id }}]" value="0">
                <input type="checkbox" class="form-check-input" value="1"
                       name="location[{{ $location->id }}]" id="location-{{ $location->id }}-1"
                        {{ $user->hasAccessToLocationId($location->id) ? 'checked' : '' }}>
                <label class="form-check-label" for="location-{{ $location->id }}-1">
                    {{ $location->name }}
                </label>
            </div>
        @endforeach
        <div>
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="{{ action([$controller, 'index'], $filter->toArray()) }}" title="Cancel"
               class="btn btn-outline-danger">Cancel</a>
        </div>
    @else
        <div class="text-muted">No locations found. Please create some.</div>
    @endif
</form>
