<?php
/**
 * @var \App\Contracts\Models\User $user
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends('layouts.app')
@section('content')
    <div class="float-end">
        <a href="{{ action([$controller, 'index'], $filter->toArray()) }}" title="Back to index">Back</a>
    </div>
    <h4>Access Administration</h4>
    <h5 class="mt-2">Type assignments for {{ $user->name() }}
        <small>[{{ $user->id }}]</small>
    </h5>
    @include('admin.access._form')
@stop
