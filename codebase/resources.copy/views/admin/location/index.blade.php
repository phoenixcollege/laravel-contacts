<?php
/**
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Training $model
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends('layouts.app')
@include('_preset.controller.index', [
    'title' => 'Location Administration',
    'filter_form_view' => null,
    'limit_columns' => ['id', 'name', 'active']
])
