<?php
/**
 * @var \Illuminate\Support\Collection $models
 */
?>
@extends('layouts.app')
@section('content')
    @include('_preset.controller._title', ['title' => 'Report Administration'])
    @includeIf('admin.report._filter_form')

    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Location</th>
                <th>Category</th>
                <th>Transaction</th>
                <th>User</th>
                <th>Updated</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($models as $model)
                <?php
                $transaction = $model->transaction;
                $category = $transaction ? $transaction->category : null;
                $location = $category ? $category->location : null;
                ?>
                <tr>
                    <td>{{ $model->id }}</td>
                    <td>{{ $location ? $location->name : '--' }}</td>
                    <td>{{ $category ? $category->category : '--' }}</td>
                    <td>{{ $transaction ? $transaction->transaction : '--' }}</td>
                    <td>{{ $model->user ? $model->user->last_name : $model->user_id }}</td>
                    <td>{{ $model->updated_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends($filter->except(['page']))->links() }}
        @endif
    @else
        <div class="text-muted">No records found.</div>
    @endif
@endsection
