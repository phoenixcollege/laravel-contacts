<?php
/**
 * @var \Smorken\IpAuth\Contracts\Models\Active $activeUser
 * @var \App\Contracts\Models\Ip $ip
 * @var \Illuminate\Support\Collection $models
 * @var \App\Contracts\Models\Category $model
 * @var \App\Contracts\Models\Transaction $transaction
 * @var \Smorken\Support\Contracts\Filter $filter
 */
?>
@extends('layouts.app')
@section('content')
    @auth
        <div class="alert alert-danger mb-2">
            A user is currently logged in. Once a computer has been authorized,
            a logged in user is not needed. Please logout.
        </div>
    @endauth
    <div class="row mb-2">
        <div class="col">
            <a href="{{ action([config('ip-auth.routes.user_select.controller', \App\Http\Controllers\UserController::class), 'select']) }}"
               title="Change active user"
               class="btn btn-outline-dark btn-block w-100">Change active user
                ({{ $activeUser?->user?->name() ?? '--' }})</a>
        </div>
    </div>
    <h4>{{ $ip?->location?->name ?? $ip->location_id }}</h4>
    <div class="mb-2" id="error-messages"></div>
    <div class="mb-2" id="last-contact">
        @include('home._last_contact')
    </div>
    @if (count($models))
        @foreach ($models as $model)
            <div class="card my-2" id="category-{{ $model->id }}">
                <div class="card-header">
                    <h5 class="card-title">{{ $model->category }}</h5>
                </div>
                <div class="card-body">
                    <div class="transactions d-flex flex-wrap justify-content-between">
                        @if ($model->transactions && count($model->transactions))
                            @foreach ($model->transactions as $transaction)
                                <div class="transaction" id="transaction-{{ $transaction->id }}">
                                    <a href="{{ action([$controller, 'add'], array_merge(['transactionId' => $transaction->id], $filter->toArray())) }}"
                                       class="btn-transaction"
                                       title="Add transaction for {{ $transaction->transaction }}">
                                        <div class="transaction-internal">
                                            @if ($transaction->image_id)
                                                <img class="transaction-image img-fluid"
                                                     src="{{ route('view.image', ['id' => $transaction->image_id]) }}"
                                                     alt="Image for {{ $transaction->transaction }}">
                                            @endif
                                            <div class="transaction-text">
                                                <span class="transaction-text-content">{{ $transaction->transaction }}</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        @else
                            <div class="text-muted">No transactions found.</div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="text-muted">No categories found. An admin needs to set up the application.</div>
    @endif
    <url id="delete-last-url" href="{{ action([$controller, 'deleteLast']) }}"></url>
@endsection
@push('add_to_end')
    <script src="{{ asset('js/limited/home.js') }}"></script>
@endpush
