<?php
/**
 * @var \App\Contracts\Models\Contact $lastContact
 */
?>
@if ($lastContact)
    Last:
    <strong>
        #{{ $lastContact->id }}
        {{ $lastContact->transaction ? $lastContact->transaction->transaction : $lastContact->transaction_id }} -
        {{ $lastContact->transaction && $lastContact->transaction->category ? $lastContact->transaction->category->category : '--' }}
    </strong>
    on {{ $lastContact->created_at }} by {{ $lastContact->user ? $lastContact->user->last_name : $lastContact->user_id }}
    <a href="{{ action([$controller, 'deleteLast']) }}" id="delete-last" title="Delete last" class="text-danger">(Delete)</a>
@endif
