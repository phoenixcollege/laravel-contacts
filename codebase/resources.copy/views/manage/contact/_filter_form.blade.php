<?php $filtered = 'border border-success'; ?>
<div class="card my-2">
    <div class="card-body">
        <form class="" method="get">
            <div class="row g-3 align-items-center mb-2">
                <div class="col-md">
                    @include('_preset.input._label', ['name' => 'f_locationId', 'title' => 'Location', 'label_classes' => 'visually-hidden'])
                    @include('_preset.input._select', [
                    'name' => 'f_locationId',
                    'classes' => $filter->f_locationId ? $filtered : '',
                    'value' => $filter->f_locationId,
                    'items' => ['' => '-- Any Location --'] + $locations->pluck('name', 'id')->all()
                    ])
                </div>
                <div class="input-group col-md">
                    <span class="input-group-text" id="start-prepend">&gt;=</span>
                    @include('_preset.input._input', [
                            'type' => 'date',
                            'name' => 'f_startAfter',
                            'classes' => $filter->f_startAfter ? $filtered : '',
                            'value' => $filter->f_startAfter,
                            'add_attrs' => [
                                'aria-describedby' => 'start-prepend',
                                'aria-label' => 'Start Date',
                            ]
                            ])
                </div>
                <div class="input-group col-md">
                    <span class="input-group-text" id="end-prepend">&lt;=</span>
                    @include('_preset.input._input', [
                            'type' => 'date',
                            'name' => 'f_startBefore',
                            'classes' => $filter->f_startBefore ? $filtered : '',
                            'value' => $filter->f_startBefore,
                            'add_attrs' => [
                                'aria-describedby' => 'end-prepend',
                                'aria-label' => 'End Date',
                            ]
                            ])
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <button type="submit" class="btn btn-primary">Filter</button>
                    <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger"
                       title="Reset filter">Reset</a>
                </div>
            </div>
        </form>
    </div>
</div>
