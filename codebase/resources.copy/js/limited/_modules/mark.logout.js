class MarkLogout {
    constructor({
                    logoutSelector = '#logout-container a',
                    classes = ['text-danger']
                } = {}) {
        this.logoutSelector = logoutSelector;
        this.classes = classes;
    }

    init() {
        const element = document.querySelector(this.logoutSelector);
        if (element) {
            element.classList.add(...this.classes);
        }
    }
}

module.exports = MarkLogout;
