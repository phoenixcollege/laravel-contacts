class LimitedMessage {
    constructor({
                    containerId = 'error-messages',
                    timeout = 10 * 1000,
                    classes = {
                        error: ['alert', 'alert-danger'],
                        info: ['alert', 'alert-info'],
                    }
                } = {}) {
        this.container = document.getElementById(containerId);
        this.classes = classes;
        this.timeout = timeout;
    }

    message(message, classes, setTimeout) {
        setTimeout = setTimeout ?? true;
        const element = document.createElement('div');
        this.addClasses(element, classes);
        this.addMessage(element, message);
        this.container.append(element);
        if (setTimeout) {
            this.setRemoveTimeout(element);
        }
    }

    setRemoveTimeout(element) {
        setTimeout(() => element.remove(), this.timeout);
    }

    addMessage(element, message) {
        element.textContent = message;
    }

    addClasses(element, classes) {
        element.classList.add(...classes);
    }

    error(message, setTimeout) {
        this.message(message, this.classes.error ?? [], setTimeout);
    }

    info(message, setTimeout) {
        this.message(message, this.classes.info ?? [], setTimeout);
    }
}

module.exports = LimitedMessage;
