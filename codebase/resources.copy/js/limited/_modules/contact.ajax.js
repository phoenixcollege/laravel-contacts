const AxiosBase = require('./axios.base');
const LimitedMessage = require('./limited.message');

class ContactAjax {

    constructor({
                    deleteLastUrl,
                    clearAlerts,
                    containerId = 'last-contact',
                    deleteLastSelector = '#delete-last',
                    addContactSelector = '.btn-transaction',
                    axios = new AxiosBase({triggerFail: false}),
                    limitedMessage = new LimitedMessage(),
                    classes = {
                        error: ['errorAnimate'],
                        success: ['successAnimate'],
                    }
                } = {}) {
        this.deleteLastUrl = deleteLastUrl;
        this.clearAlerts = clearAlerts;
        this.deleteLastSelector = deleteLastSelector;
        this.addContactSelector = addContactSelector;
        this.container = document.getElementById(containerId);
        this.axios = axios;
        this.limitedMessage = limitedMessage;
        this.classes = classes;
        this.initEvents();
    }

    initEvents() {
        const that = this;
        this.initDeleteClickEvents();
        for (const element of document.querySelectorAll(this.addContactSelector)) {
            element.addEventListener('click', function (event) {
                that.resetDom();
                that.add(element);
                event.preventDefault();
                return false;
            });
        }
    }

    initDeleteClickEvents() {
        const that = this;
        document.querySelector(this.deleteLastSelector).addEventListener('click', function (event) {
            that.resetDom();
            that.deleteLast();
            event.preventDefault();
            return false;
        });
    }

    resetDom() {
        this.container.classList.remove(...this.classes.error);
        this.container.classList.remove(...this.classes.success);
        this.clearAlerts.clear();
    }

    add(element) {
        const url = element.getAttribute('href');
        this.axios.doGet(
            url,
            (data) => this.success(data),
            (error) => this.error(error, 'Error adding contact.')
        );
    }

    deleteLast() {
        this.axios.doGet(
            this.deleteLastUrl,
            (data) => this.success(data),
            (error) => this.error(error, 'Error deleting contact.')
        );
    }

    success(data) {
        this.setContent(data.data.content);
        this.container.classList.add(...(this.classes.success ?? []));
    }

    error(error, message) {
        this.addError(message);
        this.container.classList.add(...(this.classes.error ?? []));
    }

    addError(message) {
        this.limitedMessage.error(message);
    }

    setContent(content) {
        this.container.innerHTML = content;
        this.initDeleteClickEvents();
    }
}

module.exports = ContactAjax;
