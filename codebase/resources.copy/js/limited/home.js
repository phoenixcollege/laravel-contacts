const ClearAlerts = require("./_modules/clear.alerts");
const ContactAjax = require('./_modules/contact.ajax');
const MarkLogout = require('./_modules/mark.logout');

const page = {
    deleteLastUrlId: 'delete-last-url',
    init() {
        const clearAlerts = new ClearAlerts();
        const markLogout = new MarkLogout();
        markLogout.init();
        new ContactAjax({
            deleteLastUrl: document.getElementById(page.deleteLastUrlId).getAttribute('href'),
            clearAlerts: clearAlerts
        });
    }
}
document.addEventListener('DOMContentLoaded', function () {
    page.init();
});
