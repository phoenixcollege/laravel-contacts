const ClearAlerts = require('./_modules/clear.alerts');

document.addEventListener('DOMContentLoaded', function () {
    new ClearAlerts();
});
