<?php

namespace App\IpAuth\Services\User;

use Illuminate\Http\Request;

class SelectService extends \Smorken\IpAuth\Services\User\SelectService
{
    protected function getParametersForUserGet(Request $request): array
    {
        return [
            $this->getLocationId($request),
        ];
    }

    protected function getLocationId(Request $request): ?int
    {
        $ip = $this->getIpProvider()->valid($request->ip());

        return $ip?->location?->id;
    }
}
