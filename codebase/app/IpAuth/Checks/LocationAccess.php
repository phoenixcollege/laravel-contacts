<?php

namespace App\IpAuth\Checks;

use App\Contracts\Services\Access\UserAccessService;
use Illuminate\Http\Request;
use Smorken\IpAuth\Checks\Base;
use Smorken\IpAuth\Contracts\Services\Ip\ValidIpService;

class LocationAccess extends Base
{
    /**
     * @param  UserAccessService  $handler
     */
    public function __construct(
        mixed $handler,
        protected ValidIpService $ipService
    ) {
        parent::__construct($handler);
    }

    public function isValid(string $user_id, ?Request $request, ...$params): bool
    {
        /** @var \App\Contracts\Models\Ip $ip */
        $ip = $this->ipService->findValid($request)->model;
        if ($ip) {
            return $this->handler->findLocationByUserId($user_id, $ip->location_id, false)->result;
        }

        return false;
    }
}
