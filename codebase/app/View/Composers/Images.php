<?php

namespace App\View\Composers;

use App\Contracts\Services\Images\CollectionService;
use Illuminate\Contracts\View\View;

class Images
{
    public function __construct(protected CollectionService $service)
    {
    }

    public function compose(View $view): void
    {
        $images = $this->service->active()->collection;
        $view->with('images', $images);
    }
}
