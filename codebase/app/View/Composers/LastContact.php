<?php

namespace App\View\Composers;

use App\Contracts\Services\Contacts\Home\HomeContactService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class LastContact
{
    public function __construct(protected Request $request, protected HomeContactService $service)
    {
    }

    public function compose(View $view): void
    {
        $result = $this->service->findLast($this->request);
        $view->with('lastContact', $result->model);
    }
}
