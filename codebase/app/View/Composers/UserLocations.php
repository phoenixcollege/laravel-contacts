<?php

namespace App\View\Composers;

use App\Contracts\Services\Access\UserAccessService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class UserLocations
{
    public function __construct(protected Request $request, protected UserAccessService $service)
    {
    }

    public function compose(View $view): void
    {
        $result = $this->service->getUserLocations($this->request);
        $view->with('locations', $result->collection);
    }
}
