<?php

namespace App\View\Composers;

use App\Contracts\Services\Locations\CollectionService;
use Illuminate\Contracts\View\View;

class ActiveLocations
{
    public function __construct(protected CollectionService $service)
    {
    }

    public function compose(View $view): void
    {
        $locations = $this->service->active()->collection;
        $view->with('locations', $locations);
    }
}
