<?php

namespace App\View\Composers;

use App\Contracts\Services\Categories\CollectionService;
use Illuminate\Contracts\View\View;

class ActiveCategories
{
    public function __construct(protected CollectionService $service)
    {
    }

    public function compose(View $view): void
    {
        $categories = $this->service->active()->collection;
        $view->with('categories', $categories);
    }
}
