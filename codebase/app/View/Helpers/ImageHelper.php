<?php

namespace App\View\Helpers;

use App\Contracts\Models\Image;
use Illuminate\Support\Collection;

class ImageHelper
{
    public static function toArray(Collection $images): array
    {
        $arrayed = [];
        foreach ($images as $image) {
            $arrayed[$image->id] = self::toString($image);
        }

        return $arrayed;
    }

    public static function toString(Image $image): string
    {
        return sprintf('%d - %s (%s; %d bytes)', $image->id, $image->name, $image->type, $image->size);
    }
}
