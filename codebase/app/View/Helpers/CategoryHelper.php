<?php

namespace App\View\Helpers;

use App\Contracts\Models\Category;
use Illuminate\Support\Collection;

class CategoryHelper
{
    public static function toStringWithLocation(Category $category): string
    {
        return sprintf('%s [%s]', $category->category, $category->location?->name ?? $category->location_id);
    }

    public static function toArray(Collection $categories): array
    {
        $arrayed = [];
        foreach ($categories as $category) {
            $arrayed[$category->id] = self::toStringWithLocation($category);
        }

        return $arrayed;
    }
}
