<?php

declare(strict_types=1);

namespace App\Validation\RuleProviders;

class LocationRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'name' => 'required|max:60',
            'active' => 'boolean',
            ...$overrides,
        ];
    }
}
