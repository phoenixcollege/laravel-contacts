<?php

declare(strict_types=1);

namespace App\Validation\RuleProviders;

class CategoryRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'category' => 'required',
            'location_id' => 'required|integer',
            'active' => 'boolean',
            ...$overrides,
        ];
    }
}
