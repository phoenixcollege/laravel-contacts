<?php

declare(strict_types=1);

namespace App\Validation\RuleProviders;

class IpRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'location_id' => 'required|integer',
            ...\Smorken\IpAuth\Validation\RuleProviders\IpRules::rules($overrides),
        ];
    }
}
