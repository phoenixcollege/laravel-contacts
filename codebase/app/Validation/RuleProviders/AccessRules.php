<?php

declare(strict_types=1);

namespace App\Validation\RuleProviders;

class AccessRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'user_id' => 'required',
            'location_id' => 'required|integer',
            ...$overrides,
        ];
    }
}
