<?php

declare(strict_types=1);

namespace App\Validation\RuleProviders;

class TransactionRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'transaction' => 'required|max:128',
            'category_id' => 'required|integer',
            'image_id' => 'required|integer',
            'active' => 'boolean',
            ...$overrides,
        ];
    }
}
