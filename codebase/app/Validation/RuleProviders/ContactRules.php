<?php

declare(strict_types=1);

namespace App\Validation\RuleProviders;

class ContactRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'transaction_id' => 'required',
            ...$overrides,
        ];
    }
}
