<?php

declare(strict_types=1);

namespace App\Validation\RuleProviders;

class ImageRules
{
    public static function rules(array $overrides = []): array
    {
        return [
            'image' => 'required|image',
            ...$overrides,
        ];
    }
}
