<?php

namespace App\Contracts\Services\Contacts\Admin;

use Smorken\Service\Contracts\Services\HasFilterService;

interface IndexService extends \Smorken\Service\Contracts\Services\IndexService, HasFilterService
{
}
