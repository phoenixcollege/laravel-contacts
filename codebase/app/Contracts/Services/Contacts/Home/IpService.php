<?php

namespace App\Contracts\Services\Contacts\Home;

use Illuminate\Http\Request;
use Smorken\IpAuth\Contracts\Services\Ip\ValidIpService;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\VO\ModelResult;

interface IpService extends BaseService
{
    public function findIp(Request $request): ModelResult;

    public function getValidIpService(): ValidIpService;
}
