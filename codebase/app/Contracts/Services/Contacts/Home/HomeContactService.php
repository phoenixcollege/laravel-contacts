<?php

namespace App\Contracts\Services\Contacts\Home;

use App\Contracts\Services\Access\UserAccessService;
use App\Contracts\Services\Transactions\RetrieveService;
use App\Contracts\Storage\Contact;
use Illuminate\Http\Request;
use Smorken\IpAuth\Contracts\Services\User\ActiveUserService;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\VO\ModelResult;

interface HomeContactService extends BaseService
{
    public function add(Request $request, int $transactionId): ModelResult;

    public function deleteLast(Request $request): ModelResult;

    public function deleteLastByLocationId(Request $request, int $locationId): ModelResult;

    public function findLast(Request $request): ModelResult;

    public function findLastByLocationId(Request $request, int $locationId): ModelResult;

    public function getActiveUserService(): ActiveUserService;

    public function getIpService(): IpService;

    public function getProvider(): Contact;

    public function getTransactionRetrieveService(): RetrieveService;

    public function getUserAccessService(): UserAccessService;
}
