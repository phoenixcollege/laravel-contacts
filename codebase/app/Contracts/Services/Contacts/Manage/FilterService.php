<?php

namespace App\Contracts\Services\Contacts\Manage;

use App\Contracts\Services\Access\UserAccessService;

interface FilterService extends \Smorken\Service\Contracts\Services\FilterService
{
    public function getUserAccessService(): UserAccessService;
}
