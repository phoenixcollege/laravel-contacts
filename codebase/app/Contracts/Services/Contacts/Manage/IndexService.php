<?php

namespace App\Contracts\Services\Contacts\Manage;

use Smorken\Service\Contracts\Services\HasFilterService;

interface IndexService extends \Smorken\Service\Contracts\Services\IndexService, HasFilterService
{
}
