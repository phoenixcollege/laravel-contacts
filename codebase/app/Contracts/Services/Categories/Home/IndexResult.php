<?php

namespace App\Contracts\Services\Categories\Home;

/**
 * @property \App\Contracts\Models\Ip $ip
 * @property \Illuminate\Support\Collection|\Illuminate\Contracts\Pagination\LengthAwarePaginator $models
 * @property \Smorken\Support\Contracts\Filter|null $filter
 */
interface IndexResult extends \Smorken\Service\Contracts\Services\VO\IndexResult
{
}
