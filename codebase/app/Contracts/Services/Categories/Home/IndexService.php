<?php

namespace App\Contracts\Services\Categories\Home;

use App\Contracts\Services\Contacts\Home\IpService;
use App\Contracts\Storage\Category;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\HasFilterService;

interface IndexService extends BaseService, HasFilterService
{
    public function getByRequest(Request $request): IndexResult;

    public function getByRequestAndLocationId(Request $request, int $locationId): IndexResult;

    public function getIpService(): IpService;

    public function getProvider(): Category;
}
