<?php

namespace App\Contracts\Services\Categories;

use App\Contracts\Storage\Category;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\VO\CollectionResult;

interface CollectionService extends BaseService
{
    public function active(): CollectionResult;

    public function getProvider(): Category;
}
