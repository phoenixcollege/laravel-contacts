<?php

namespace App\Contracts\Services\Images;

use App\Contracts\Storage\Image;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;

interface ViewService extends BaseService
{
    public function getProvider(): Image;

    public function view(Request $request, int $id): ViewResult;
}
