<?php

namespace App\Contracts\Services\Images;

interface HeadersHelper
{
    public function getExpires(): string;

    public function getHeaders(): array;

    public function getViewResult(): ViewResult;
}
