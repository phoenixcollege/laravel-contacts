<?php

namespace App\Contracts\Services\Images;

use Illuminate\Http\Response;
use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \App\Contracts\Models\VO\Image $image
 */
interface ViewResult extends VOResult
{
    public function getHeadersHelper(): HeadersHelper;

    public function getMime(): string;

    public function getName(): string;

    public function toResponse(bool $encode = false): Response;

    public static function setHeadersHelperClass(string $headersHelperClass): void;
}
