<?php

namespace App\Contracts\Services\Images;

use App\Contracts\Storage\Image;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\VO\CollectionResult;

interface CollectionService extends BaseService
{
    public function active(): CollectionResult;

    public function getProvider(): Image;
}
