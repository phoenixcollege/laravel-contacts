<?php

namespace App\Contracts\Services\Locations;

use App\Contracts\Storage\Location;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\VO\CollectionResult;

interface CollectionService extends BaseService
{
    public function active(): CollectionResult;

    public function getProvider(): Location;
}
