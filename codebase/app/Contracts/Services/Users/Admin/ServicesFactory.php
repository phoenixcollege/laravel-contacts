<?php

namespace App\Contracts\Services\Users\Admin;

use Smorken\Service\Contracts\Services\Factory;

interface ServicesFactory extends Factory
{
    public function getAccessService(): AccessService;

    public function getIndexService(): IndexService;
}
