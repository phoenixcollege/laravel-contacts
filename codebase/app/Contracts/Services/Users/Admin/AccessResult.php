<?php

namespace App\Contracts\Services\Users\Admin;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \App\Contracts\Models\User $user
 * @property bool $updated
 */
interface AccessResult extends VOResult
{
}
