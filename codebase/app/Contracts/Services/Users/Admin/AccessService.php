<?php

namespace App\Contracts\Services\Users\Admin;

use App\Contracts\Storage\User;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\HasFilterService;
use Smorken\Service\Contracts\Services\VO\ModelResult;

interface AccessService extends BaseService, HasFilterService
{
    public function assignmentsFromArray(string|int $userId, array $locations): AccessResult;

    public function assignmentsFromRequest(Request $request, string|int $userId): AccessResult;

    public function findUser(int|string $userId): ModelResult;

    public function getProvider(): User;
}
