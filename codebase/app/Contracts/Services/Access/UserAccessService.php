<?php

namespace App\Contracts\Services\Access;

use App\Contracts\Storage\Access;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Service\Contracts\Services\VO\CollectionResult;
use Smorken\Service\Contracts\Services\VO\ModelResult;

interface UserAccessService extends BaseService
{
    public function findLocation(Request $request, int $locationId, bool $shouldThrow = true): ModelResult;

    public function findLocationByUserId(string|int $userId, int $locationId, bool $shouldThrow = true): ModelResult;

    public function getProvider(): Access;

    public function getUserLocations(Request $request): CollectionResult;

    public function getUserLocationsByUserId(string|int $userId): CollectionResult;
}
