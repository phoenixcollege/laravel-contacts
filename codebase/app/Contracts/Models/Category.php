<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/15/18
 * Time: 8:07 AM
 */

namespace App\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Category
 *
 *
 * @property int $id
 * @property int $location_id
 * @property string $category
 * @property bool $active
 * @property \App\Enums\Active $active_enum
 * @property string $active_string
 * @property \App\Contracts\Models\Location $location
 */
interface Category extends Model
{
}
