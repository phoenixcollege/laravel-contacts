<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/9/18
 * Time: 12:49 PM
 */

namespace App\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Access
 *
 *
 * @property int $id
 * @property string $user_id
 * @property int $location_id
 * @property \App\Contracts\Models\Location $location
 */
interface Access extends Model
{
}
