<?php

namespace App\Contracts\Models\VO;

/**
 * @property int $id
 * @property string $mime
 * @property string $data
 * @property int $size
 */
interface Image
{
    public static function emptyImage(): static;

    public static function fromImage(\App\Contracts\Models\Image $image): static;
}
