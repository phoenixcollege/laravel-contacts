<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/5/18
 * Time: 2:29 PM
 */

namespace App\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Location
 *
 *
 * @property int $id
 * @property string $name
 * @property bool $active
 * @property \App\Enums\Active $active_enum
 * @property string $active_string
 */
interface Location extends Model
{
}
