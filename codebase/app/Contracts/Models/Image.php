<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/15/18
 * Time: 8:36 AM
 */

namespace App\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Image
 *
 *
 * @property int $id
 * @property mixed $image
 * @property int $size
 * @property string $name
 * @property string $type
 */
interface Image extends Model
{
    public function getSrcString(): string;
}
