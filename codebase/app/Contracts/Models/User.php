<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/14/18
 * Time: 10:17 AM
 */

namespace App\Contracts\Models;

interface User extends \Smorken\IpAuth\Contracts\Models\User
{
    public function hasAccessToLocationId(int $locationId): bool;
}
