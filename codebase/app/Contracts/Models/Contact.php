<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/16/18
 * Time: 8:48 AM
 */

namespace App\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Contact
 *
 *
 * @property int $id
 * @property string $user_id
 * @property int $transaction_id
 *
 * Virtual
 * @property int $location_id  from ->transaction->category
 *
 * Relations
 * @property \App\Contracts\Models\User $user
 * @property \App\Contracts\Models\Transaction $transaction
 */
interface Contact extends Model
{
}
