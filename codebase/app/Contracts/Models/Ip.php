<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/9/18
 * Time: 9:06 AM
 */

namespace App\Contracts\Models;

/**
 * Interface Ip
 *
 *
 * @property int $id
 * @property string $ip
 * @property \Carbon\Carbon $valid_until
 * @property string $user_id
 * @property int $location_id
 * @property \App\Contracts\Models\Location $location
 */
interface Ip extends \Smorken\IpAuth\Contracts\Models\Ip
{
}
