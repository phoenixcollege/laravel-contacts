<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/15/18
 * Time: 8:34 AM
 */

namespace App\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface Transaction
 *
 *
 * @property int $id
 * @property string $transaction
 * @property int $category_id
 * @property int $image_id
 * @property bool $active
 * @property \App\Enums\Active $active_enum
 * @property string $active_string
 * @property \App\Contracts\Models\Category $category
 * @property \App\Contracts\Models\Image $image
 */
interface Transaction extends Model
{
}
