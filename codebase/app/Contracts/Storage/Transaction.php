<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/15/18
 * Time: 8:37 AM
 */

namespace App\Contracts\Storage;

use Smorken\Storage\Contracts\Base;

interface Transaction extends Base
{
}
