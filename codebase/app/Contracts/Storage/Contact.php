<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/16/18
 * Time: 8:50 AM
 */

namespace App\Contracts\Storage;

use Smorken\Storage\Contracts\Base;

interface Contact extends Base
{
    public function add(string|int $userId, int $transactionId): \App\Contracts\Models\Contact;

    public function deleteLastByLocationId(int $locationId): bool;

    public function lastByLocationId(int $locationId): ?\App\Contracts\Models\Contact;
}
