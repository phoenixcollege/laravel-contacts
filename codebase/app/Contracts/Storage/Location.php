<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/5/18
 * Time: 2:29 PM
 */

namespace App\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Storage\Contracts\Base;

/**
 * Interface Location
 */
interface Location extends Base
{
    public function active(): Collection;
}
