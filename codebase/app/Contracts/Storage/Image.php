<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/15/18
 * Time: 8:54 AM
 */

namespace App\Contracts\Storage;

use Smorken\Storage\Contracts\Base;

interface Image extends Base
{
    public function updateName(int $id, string $name): \App\Contracts\Models\Image;
}
