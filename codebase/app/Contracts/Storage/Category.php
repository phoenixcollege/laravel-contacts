<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/15/18
 * Time: 8:08 AM
 */

namespace App\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Storage\Contracts\Base;

interface Category extends Base
{
    public function active(): Collection;

    public function getByLocationId(int $locationId): Collection;
}
