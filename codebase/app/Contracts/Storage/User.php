<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/14/18
 * Time: 10:16 AM
 */

namespace App\Contracts\Storage;

use Smorken\Storage\Contracts\Base;

interface User extends \Smorken\IpAuth\Contracts\Storage\User, Base
{
    public function handleLocationAssignments(\App\Contracts\Models\User $user, array $locations): bool;
}
