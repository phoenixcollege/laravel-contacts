<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/14/18
 * Time: 10:07 AM
 */

namespace App\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Storage\Contracts\Base;

interface Access extends Base
{
    public function accessByUserIdAndLocationId(string|int $userId, int $locationId): ?\App\Contracts\Models\Access;

    public function getAccessByUserId(string|int $userId): Collection;
}
