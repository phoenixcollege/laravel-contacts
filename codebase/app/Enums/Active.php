<?php

namespace App\Enums;

enum Active: int
{
    case ACTIVE = 1;
    case INACTIVE = 0;

    public static function toArray(): array
    {
        return [
            self::ACTIVE->value => 'Active',
            self::INACTIVE->value => 'Inactive',
        ];
    }

    public function text(): string
    {
        return self::toArray()[$this->value];
    }
}
