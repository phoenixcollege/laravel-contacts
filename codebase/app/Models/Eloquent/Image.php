<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/15/18
 * Time: 8:41 AM
 */

namespace App\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Image extends Base implements \App\Contracts\Models\Image
{
    protected $fillable = ['image', 'size', 'name', 'type'];

    public function __toString(): string
    {
        return sprintf('%s [%s bytes, %s]', $this->name, $this->size, $this->type);
    }

    public function getSrcString(): string
    {
        return sprintf('data:%s;base64, %s', $this->type, base64_encode($this->image));
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('name');
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query;
    }

    public function scopeSkipImage(Builder $query): Builder
    {
        return $query->select(['id', 'size', 'name', 'type']);
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }
}
