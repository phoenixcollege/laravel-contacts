<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/9/18
 * Time: 12:47 PM
 */

namespace App\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Concerns\WithFriendlyKeys;

class User extends \Smorken\IpAuth\Models\Eloquent\User\Base implements \App\Contracts\Models\User
{
    use WithFriendlyKeys;

    protected array $accessToLocations = [];

    protected array $friendlyKeys = [
        'id' => 'ID',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'email' => 'Email',
    ];

    public function accesses(): HasMany
    {
        return $this->hasMany(Access::class);
    }

    public function hasAccessToLocationId(int $locationId): bool
    {
        if (! $this->accessToLocations) {
            foreach ($this->accesses as $access) {
                $this->accessToLocations[$access->location_id] = true;
            }
        }

        return array_key_exists($locationId, $this->accessToLocations);
    }

    public function locations(): BelongsToMany
    {
        return $this->belongsToMany(Location::class, 'accesses');
    }

    public function scopeAccessHasLocationId(Builder $query, string|int $locationId): Builder
    {
        if ($locationId === '-') {
            return $query->doesntHave('accesses');
        }

        return $query->whereHas(
            'accesses',
            function ($sq) use ($locationId) {
                $sq->where('location_id', '=', $locationId);
            }
        );
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query->with('accesses.location');
    }

    public function scopeFirstNameLike(Builder $query, string $value): Builder
    {
        return $query->where('first_name', 'LIKE', $value.'%');
    }

    public function scopeLastNameLike(Builder $query, string $value): Builder
    {
        return $query->where('last_name', 'LIKE', $value.'%');
    }
}
