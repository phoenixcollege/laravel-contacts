<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 6/5/18
 * Time: 2:33 PM
 */

namespace App\Models\Eloquent;

use App\Models\Extensions\ActiveTrait;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Model\Concerns\WithFriendlyKeys;

class Location extends Base implements \App\Contracts\Models\Location
{
    use ActiveTrait, WithFriendlyKeys;

    protected $fillable = ['name', 'active'];

    protected array $friendlyKeys = [
        'id' => 'ID',
        'name' => 'Name',
        'active' => 'Active',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('active', 'desc')
            ->orderBy('name');
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query;
    }

    public function scopeIsActive(Builder $query): Builder
    {
        return $query->where('active', '=', 1);
    }
}
