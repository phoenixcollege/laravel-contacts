<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/15/18
 * Time: 8:38 AM
 */

namespace App\Models\Eloquent;

use App\Models\Extensions\ActiveTrait;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Smorken\Model\Concerns\WithFriendlyKeys;

class Transaction extends Base implements \App\Contracts\Models\Transaction
{
    use ActiveTrait, WithFriendlyKeys;

    protected $fillable = ['transaction', 'category_id', 'image_id', 'active'];

    protected array $friendlyKeys = [
        'id' => 'ID',
        'transaction' => 'Transaction',
        'category_id' => 'Category ID',
        'image_id' => 'Image ID',
        'active' => 'Active',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function image(): BelongsTo
    {
        return $this->belongsTo(Image::class);
    }

    public function scopeActiveIs(Builder $query, int $active): Builder
    {
        return $query->where('active', '=', $active);
    }

    public function scopeCategoryIdIs(Builder $query, int $categoryId): Builder
    {
        return $query->where('category_id', '=', $categoryId);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('active', 'desc')
            ->orderBy('category_id')
            ->orderBy('transaction');
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query->with('category')
            ->with('image:id,name,size,type');
    }
}
