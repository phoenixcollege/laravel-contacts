<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/9/18
 * Time: 9:07 AM
 */

namespace App\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Ip extends \Smorken\IpAuth\Models\Eloquent\Ip implements \App\Contracts\Models\Ip
{
    protected $fillable = ['ip', 'valid_until', 'user_id', 'location_id'];

    public function location(): BelongsTo
    {
        return $this->belongsTo(Location::class);
    }

    public function scopeLocationIdIs(Builder $query, int $id): Builder
    {
        return $query->where('location_id', '=', $id);
    }
}
