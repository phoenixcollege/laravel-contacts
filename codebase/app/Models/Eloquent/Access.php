<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/9/18
 * Time: 12:50 PM
 */

namespace App\Models\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Access extends Base implements \App\Contracts\Models\Access
{
    protected $fillable = ['user_id', 'location_id'];

    public function location(): BelongsTo
    {
        return $this->belongsTo(Location::class);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query;
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query->with('location');
    }

    public function scopeHasActiveLocation(Builder $query): Builder
    {
        return $query->whereHas('location', function (Builder $q) {
            $q->isActive(1);
        });
    }

    public function scopeLocationIdIs(Builder $query, int $id): Builder
    {
        return $query->where('location_id', '=', $id);
    }

    public function scopeUserIdIs(Builder $query, string|int $id): Builder
    {
        return $query->where('user_id', '=', $id);
    }
}
