<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/16/18
 * Time: 8:50 AM
 */

namespace App\Models\Eloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Contact extends Base implements \App\Contracts\Models\Contact
{
    protected $fillable = ['user_id', 'transaction_id'];

    public function getLocationIdAttribute(): ?int
    {
        return $this->transaction?->category?->location_id;
    }

    public function scopeCreatedAfter(Builder $query, Carbon|string $date): Builder
    {
        return $query->whereDate('created_at', '>=', $date);
    }

    public function scopeCreatedBefore(Builder $query, Carbon|string $date): Builder
    {
        return $query->whereDate('created_at', '<=', $date);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('created_at', 'desc');
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query->with('transaction.category.location')
            ->with('user');
    }

    public function scopeHasLocationId(Builder $query, int $locationId): Builder
    {
        return $query->whereHas(
            'transaction',
            function ($sq) use ($locationId) {
                $sq->whereHas(
                    'category',
                    function ($ssq) use ($locationId) {
                        $ssq->where('location_id', '=', $locationId);
                    }
                );
            }
        );
    }

    public function scopeLimitOne(Builder $query): Builder
    {
        return $query->limit(1);
    }

    public function transaction(): BelongsTo
    {
        return $this->belongsTo(Transaction::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
