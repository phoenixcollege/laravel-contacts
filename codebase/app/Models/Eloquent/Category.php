<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 8/15/18
 * Time: 8:08 AM
 */

namespace App\Models\Eloquent;

use App\Models\Extensions\ActiveTrait;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Model\Concerns\WithFriendlyKeys;

class Category extends Base implements \App\Contracts\Models\Category
{
    use ActiveTrait, WithFriendlyKeys;

    protected $fillable = ['category', 'location_id', 'active'];

    protected array $friendlyKeys = [
        'id' => 'ID',
        'category' => 'Category',
        'location_id' => 'Location ID',
        'active' => 'Active',
        'created_at' => 'Created',
        'updated_at' => 'Updated',
    ];

    public function location(): BelongsTo
    {
        return $this->belongsTo(Location::class);
    }

    public function scopeActiveIs(Builder $query, int $active): Builder
    {
        return $query->where('active', '=', $active);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $query->orderBy('active', 'desc')
            ->orderBy('location_id')
            ->orderBy('category');
    }

    public function scopeDefaultWiths(Builder $query): Builder
    {
        return $query->with('location')
            ->with('transactions');
    }

    public function scopeLocationIdIs(Builder $query, int $locationId): Builder
    {
        return $query->where('location_id', '=', $locationId);
    }

    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class)
            ->where('active', '=', 1)
            ->orderBy('transaction');
    }
}
