<?php

namespace App\Models\Extensions;

use App\Enums\Active;

trait ActiveTrait
{
    public function getActiveEnumAttribute(): Active
    {
        return Active::from($this->active);
    }

    public function getActiveStringAttribute(): string
    {
        return $this->getActiveEnumAttribute()->text();
    }
}
