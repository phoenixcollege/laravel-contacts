<?php

namespace App\Models\VO;

class Image implements \App\Contracts\Models\VO\Image
{
    public function __construct(
        public int $id,
        public string $mime,
        public string $data,
        public int $size
    ) {
    }

    public static function emptyImage(): static
    {
        return new static(0, 'image/png', '', 0);
    }

    public static function fromImage(\App\Contracts\Models\Image $image): static
    {
        return new static($image->id, $image->type, $image->image, $image->size);
    }
}
