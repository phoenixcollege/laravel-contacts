<?php

namespace App\Providers;

use App\View\Composers\ActiveCategories;
use App\View\Composers\ActiveLocations;
use App\View\Composers\Images;
use App\View\Composers\LastContact;
use App\View\Composers\UserLocations;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Smorken\IpAuth\View\Composers\ActiveIp;
use Smorken\IpAuth\View\Composers\ActiveUser;

class ViewComposerServiceProvider extends ServiceProvider
{
    protected array $viewComposers = [
        ActiveCategories::class => [
            'admin.transaction._filter_form',
            'admin.transaction._form',
        ],
        ActiveIp::class => [
            'smorken/ip-auth::user.select',
        ],
        ActiveLocations::class => [
            'admin.access._filter_form',
            'admin.access._form',
            'admin.category._form',
            'admin.category._filter_form',
            'admin.report._filter_form',
        ],
        ActiveUser::class => [
            'home.index',
        ],
        Images::class => [
            'admin.transaction._form',
        ],
        LastContact::class => [
            'home._last_contact',
        ],
        UserLocations::class => [
            'smorken/ip-auth::authorize.index',
            'manage.contact._filter_form',
        ],
    ];

    public function boot(): void
    {
        foreach ($this->viewComposers as $className => $views) {
            $this->bootViewComposers($className, $views);
        }
    }

    protected function bootViewComposers(string $className, array $views): void
    {
        View::composer($views, $className);
    }
}
