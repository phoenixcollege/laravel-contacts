<?php

namespace App\Providers\Services\Manage;

use App\Contracts\Services\Access\UserAccessService;
use App\Contracts\Services\Contacts\Manage\CrudServices;
use App\Contracts\Services\Contacts\Manage\IndexService;
use App\Contracts\Storage\Contact;
use App\Services\Contacts\Manage\UserAccessPoliciesService;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\GatePoliciesService;
use Smorken\Service\Contracts\Services\GateService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;
use Smorken\Service\Services\GateWithGatePoliciesService;

class ContactServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Contact::class];
            $additionalServices = [
                FilterService::class => new \App\Services\Contacts\Manage\FilterService([
                    UserAccessService::class => $app[UserAccessService::class],
                ]),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
                GateService::class => $this->getGateService($app),
            ];

            return \App\Services\Contacts\Manage\CrudServices::createByStorageProvider($provider, $additionalServices);
        });
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Contacts\Manage\IndexService(
                $app[Contact::class],
                [
                    FilterService::class => new \App\Services\Contacts\Manage\FilterService([
                        UserAccessService::class => $app[UserAccessService::class],
                    ]),
                    GateService::class => $this->getGateService($app),
                ]
            );
        });
    }

    protected function getGateService(Application $app): GateService
    {
        return new GateWithGatePoliciesService(
            $app[Gate::class],
            [
                GatePoliciesService::class => new UserAccessPoliciesService([
                    UserAccessService::class => $app[UserAccessService::class],
                ]),
            ]
        );
    }
}
