<?php

namespace App\Providers\Services;

use App\Contracts\Services\Images\CollectionService;
use App\Contracts\Services\Images\ViewService;
use App\Contracts\Storage\Image;
use Smorken\Service\Invokables\Invokable;

class ImageServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(CollectionService::class, function ($app) {
            return new \App\Services\Images\CollectionService(
                $app[Image::class]
            );
        });
        $this->getApp()->bind(ViewService::class, function ($app) {
            return new \App\Services\Images\ViewService(
                $app[Image::class]
            );
        });
    }
}
