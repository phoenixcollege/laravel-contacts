<?php

namespace App\Providers\Services;

use App\Contracts\Services\Categories\CollectionService;
use App\Contracts\Storage\Category;
use Smorken\Service\Invokables\Invokable;

class CategoryServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(CollectionService::class, function ($app) {
            return new \App\Services\Categories\CollectionService(
                $app[Category::class]
            );
        });
    }
}
