<?php

namespace App\Providers\Services\Home;

use App\Contracts\Services\Access\UserAccessService;
use App\Contracts\Services\Contacts\Home\HomeContactService;
use App\Contracts\Services\Contacts\Home\IpService;
use App\Contracts\Services\Transactions\RetrieveService;
use App\Contracts\Storage\Contact;
use Smorken\IpAuth\Contracts\Services\Ip\ValidIpService;
use Smorken\IpAuth\Contracts\Services\User\ActiveUserService;
use Smorken\Service\Invokables\Invokable;

class ContactServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(HomeContactService::class, function ($app) {
            $services = [
                ActiveUserService::class => $app[ActiveUserService::class],
                IpService::class => $app[IpService::class],
                RetrieveService::class => $app[RetrieveService::class],
                UserAccessService::class => $app[UserAccessService::class],
            ];

            return new \App\Services\Contacts\Home\HomeContactService(
                $app[Contact::class],
                $services
            );
        });
        $this->getApp()->bind(IpService::class, function ($app) {
            return new \App\Services\Contacts\Home\IpService([
                ValidIpService::class => $app[ValidIpService::class],
            ]);
        });
    }
}
