<?php

namespace App\Providers\Services\Home;

use App\Contracts\Services\Categories\Home\IndexService;
use App\Contracts\Services\Contacts\Home\IpService;
use App\Contracts\Storage\Category;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Invokables\Invokable;

class CategoryServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Categories\Home\IndexService(
                $app[Category::class],
                [
                    FilterService::class => new \App\Services\Categories\Home\FilterService(),
                    IpService::class => $app[IpService::class],
                ]
            );
        });
    }
}
