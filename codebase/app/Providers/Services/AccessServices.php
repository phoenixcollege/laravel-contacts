<?php

namespace App\Providers\Services;

use App\Contracts\Services\Access\UserAccessService;
use App\Contracts\Storage\Access;
use Smorken\Service\Invokables\Invokable;

class AccessServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(UserAccessService::class, function ($app) {
            return new \App\Services\Access\UserAccessService(
                $app[Access::class]
            );
        });
    }
}
