<?php

namespace App\Providers\Services;

use App\Contracts\Services\Transactions\RetrieveService;
use App\Contracts\Storage\Transaction;
use Smorken\Service\Invokables\Invokable;

class TransactionServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(RetrieveService::class, function ($app) {
            return new \App\Services\Transactions\RetrieveService(
                $app[Transaction::class]
            );
        });
    }
}
