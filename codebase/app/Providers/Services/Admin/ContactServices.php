<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Contacts\Admin\ExportService;
use App\Contracts\Services\Contacts\Admin\IndexService;
use App\Contracts\Storage\Contact;
use Smorken\Export\Contracts\Export;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Invokables\Invokable;

class ContactServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(ExportService::class, function ($app) {
            return new \App\Services\Contacts\Admin\ExportService(
                $app[Export::class],
                $app[Contact::class],
                [
                    FilterService::class => new \App\Services\Contacts\Admin\FilterService(),
                ]
            );
        });
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Contacts\Admin\IndexService(
                $app[Contact::class],
                [
                    FilterService::class => new \App\Services\Contacts\Admin\FilterService(),
                ]
            );
        });
    }
}
