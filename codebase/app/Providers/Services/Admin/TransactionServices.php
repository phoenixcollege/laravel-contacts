<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Transactions\Admin\CrudServices;
use App\Contracts\Services\Transactions\Admin\IndexService;
use App\Contracts\Storage\Transaction;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class TransactionServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Transactions\Admin\IndexService(
                $app[Transaction::class],
                [
                    FilterService::class => new \App\Services\Transactions\Admin\FilterService(),
                ]
            );
        });
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Transaction::class];
            $additionalServices = [
                FilterService::class => new \App\Services\Transactions\Admin\FilterService(),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];

            return \App\Services\Transactions\Admin\CrudServices::createByStorageProvider($provider,
                $additionalServices);
        });
    }
}
