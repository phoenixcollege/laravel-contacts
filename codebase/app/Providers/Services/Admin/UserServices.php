<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Users\Admin\AccessService;
use App\Contracts\Services\Users\Admin\IndexService;
use App\Contracts\Services\Users\Admin\ServicesFactory;
use App\Contracts\Storage\User;
use App\Services\Users\Admin\FilterService;
use Smorken\Service\Invokables\Invokable;

class UserServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(ServicesFactory::class, function ($app) {
            $user = $app[User::class];
            $filterService = new FilterService();
            $services = [
                AccessService::class => new \App\Services\Users\Admin\AccessService(
                    $user
                ),
                IndexService::class => new \App\Services\Users\Admin\IndexService(
                    $user
                ),
            ];
            $additionalServices = [
                \Smorken\Service\Contracts\Services\FilterService::class => $filterService,
            ];

            return new \App\Services\Users\Admin\ServicesFactory($services, $additionalServices);
        });
    }
}
