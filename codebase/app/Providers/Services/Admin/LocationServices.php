<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Locations\Admin\CrudServices;
use App\Contracts\Services\Locations\Admin\IndexService;
use App\Contracts\Storage\Location;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class LocationServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Locations\Admin\IndexService(
                $app[Location::class],
                [
                    FilterService::class => new \App\Services\Locations\Admin\FilterService(),
                ]
            );
        });
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Location::class];
            $additionalServices = [
                FilterService::class => new \App\Services\Locations\Admin\FilterService(),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];

            return \App\Services\Locations\Admin\CrudServices::createByStorageProvider($provider, $additionalServices);
        });
    }
}
