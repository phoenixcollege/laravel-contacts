<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Categories\Admin\CrudServices;
use App\Contracts\Services\Categories\Admin\IndexService;
use App\Contracts\Storage\Category;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class CategoryServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Categories\Admin\IndexService(
                $app[Category::class],
                [
                    FilterService::class => new \App\Services\Categories\Admin\FilterService(),
                ]
            );
        });
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Category::class];
            $additionalServices = [
                FilterService::class => new \App\Services\Categories\Admin\FilterService(),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
            ];

            return \App\Services\Categories\Admin\CrudServices::createByStorageProvider($provider, $additionalServices);
        });
    }
}
