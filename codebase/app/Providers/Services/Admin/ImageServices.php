<?php

namespace App\Providers\Services\Admin;

use App\Contracts\Services\Images\Admin\CrudServices;
use App\Contracts\Services\Images\Admin\IndexService;
use App\Contracts\Storage\Image;
use App\Services\Images\Admin\CreateService;
use App\Services\Images\Admin\UpdateService;
use Illuminate\Contracts\Validation\Factory;
use Smorken\Image\Sizer\Contracts\Sizer;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\RequestService;
use Smorken\Service\Contracts\Services\ValidationRulesService;
use Smorken\Service\Contracts\Services\ValidatorService;
use Smorken\Service\Invokables\Invokable;

class ImageServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(IndexService::class, function ($app) {
            return new \App\Services\Images\Admin\IndexService(
                $app[Image::class],
                [
                    FilterService::class => new \App\Services\Images\Admin\FilterService(),
                ]
            );
        });
        $this->getApp()->bind(CrudServices::class, function ($app) {
            $provider = $app[Image::class];
            $sizer = $app[Sizer::class];
            $additionalServices = [
                FilterService::class => new \App\Services\Images\Admin\FilterService(),
                RequestService::class => new \App\Services\Images\Admin\RequestService(),
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
                ValidationRulesService::class => new \App\Services\Images\Admin\ValidationRulesService($provider),
            ];
            $cs = \App\Services\Images\Admin\CrudServices::createByStorageProvider($provider, $additionalServices);
            $cs->setCreateService(new CreateService($sizer, $provider, $additionalServices));
            $cs->setUpdateService(new UpdateService($sizer, $provider, $additionalServices));

            return $cs;
        });
    }
}
