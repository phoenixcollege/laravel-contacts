<?php

namespace App\Providers\Services;

use App\Contracts\Services\Locations\CollectionService;
use App\Contracts\Storage\Location;
use Smorken\Service\Invokables\Invokable;

class LocationServices extends Invokable
{
    public function __invoke(): void
    {
        $this->getApp()->bind(CollectionService::class, function ($app) {
            return new \App\Services\Locations\CollectionService(
                $app[Location::class]
            );
        });
    }
}
