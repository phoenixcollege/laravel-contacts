<?php

namespace App\Services\Categories\Home;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        $filter = new \Smorken\Support\Filter([
            'f_active' => 1,
        ]);
        $filter->hide(['f_locationId', 'f_active']);

        return $filter;
    }
}
