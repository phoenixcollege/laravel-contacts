<?php

namespace App\Services\Categories\Home;

use App\Contracts\Models\Ip;
use App\Contracts\Services\Categories\Home\IndexResult;
use App\Contracts\Services\Contacts\Home\IpService;
use App\Contracts\Storage\Category;
use App\Services\Extensions\LocalCacheTrait;
use Illuminate\Http\Request;
use Smorken\Service\Services\BaseService;
use Smorken\Service\Services\Traits\HasFilterServiceTrait;

class IndexService extends BaseService implements \App\Contracts\Services\Categories\Home\IndexService
{
    use HasFilterServiceTrait, LocalCacheTrait;

    public function __construct(protected Category $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function getByRequest(Request $request): IndexResult
    {
        /** @var \App\Contracts\Models\Ip $ip */
        $ip = $this->findIp($request);

        return $this->getByRequestAndLocationId($request, $ip->location_id);
    }

    public function getByRequestAndLocationId(Request $request, int $locationId): IndexResult
    {
        $filter = $this->getFilterService()->getFilterFromRequest($request);
        $filter->f_locationId = $locationId;
        $models = $this->getProvider()->getByFilter($filter, 0);

        return new \App\Services\Categories\Home\IndexResult(
            $this->findIp($request),
            $models,
            $filter
        );
    }

    public function getIpService(): IpService
    {
        return $this->getService(IpService::class);
    }

    public function getProvider(): Category
    {
        return $this->provider;
    }

    protected function findIp(Request $request): ?Ip
    {
        $ip = $this->fromCache('ip');
        if ($ip) {
            return $ip;
        }
        $ip = $this->getIpService()->findIp($request)->model;
        $this->setCache('ip', $ip);

        return $ip;
    }
}
