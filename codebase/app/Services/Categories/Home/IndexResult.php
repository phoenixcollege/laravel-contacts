<?php

namespace App\Services\Categories\Home;

use App\Contracts\Models\Ip;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Smorken\Support\Contracts\Filter;

class IndexResult extends \Smorken\Service\Services\VO\IndexResult implements \App\Contracts\Services\Categories\Home\IndexResult
{
    public function __construct(
        public Ip $ip,
        LengthAwarePaginator|Collection $models,
        Filter $filter = null
    ) {
        parent::__construct($models, $filter);
    }
}
