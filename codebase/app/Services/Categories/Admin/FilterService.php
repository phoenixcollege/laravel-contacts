<?php

namespace App\Services\Categories\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'f_locationId' => $request->input('f_locationId'),
            'f_active' => $request->input('f_active'),
        ]);
    }
}
