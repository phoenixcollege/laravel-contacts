<?php

namespace App\Services\Categories;

use App\Contracts\Storage\Category;
use Smorken\Service\Contracts\Services\VO\CollectionResult;
use Smorken\Service\Services\BaseService;

class CollectionService extends BaseService implements \App\Contracts\Services\Categories\CollectionService
{
    public function __construct(protected Category $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function active(): CollectionResult
    {
        return new \Smorken\Service\Services\VO\CollectionResult(
            $this->getProvider()->active()
        );
    }

    public function getProvider(): Category
    {
        return $this->provider;
    }
}
