<?php

namespace App\Services\Images;

use App\Contracts\Storage\Image;
use Smorken\Service\Contracts\Services\VO\CollectionResult;
use Smorken\Service\Services\BaseService;

class CollectionService extends BaseService implements \App\Contracts\Services\Images\CollectionService
{
    public function __construct(protected Image $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function active(): CollectionResult
    {
        $images = $this->getProvider()->all();

        return new \Smorken\Service\Services\VO\CollectionResult($images);
    }

    public function getProvider(): Image
    {
        return $this->provider;
    }
}
