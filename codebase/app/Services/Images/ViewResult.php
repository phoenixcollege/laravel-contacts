<?php

namespace App\Services\Images;

use App\Contracts\Models\VO\Image;
use App\Contracts\Services\Images\HeadersHelper;
use Illuminate\Http\Response;
use Smorken\Service\Services\VO\VOResult;

class ViewResult extends VOResult implements \App\Contracts\Services\Images\ViewResult
{
    protected static string $headerHelperClass = \App\Services\Images\Helpers\HeadersHelper::class;

    public function __construct(
        public ?Image $image
    ) {
    }

    public static function setHeadersHelperClass(string $headersHelperClass): void
    {
        self::$headerHelperClass = $headersHelperClass;
    }

    public function getHeadersHelper(): HeadersHelper
    {
        return new self::$headerHelperClass($this);
    }

    public function getMime(): string
    {
        return $this->image?->mime ?? 'image/png';
    }

    public function getName(): string
    {
        return sprintf('%d.%s', $this->image?->id ?? 0, $this->getImageExtension());
    }

    public function toResponse(bool $encode = false): Response
    {
        $headers = $this->getHeadersHelper()->getHeaders();

        return \Illuminate\Support\Facades\Response::make(
            $this->getData($encode),
            $this->image ? 200 : 404,
            $headers
        );
    }

    protected function getData(bool $encode): string
    {
        if ($this->image) {
            return $encode ? base64_encode($this->image->data) : $this->image->data;
        }

        return '';
    }

    protected function getImageExtension(): string
    {
        $mime = $this->getMime();
        if (str_ends_with($mime, 'jpg') || str_ends_with($mime, 'jpeg')) {
            return 'jpg';
        }

        return 'png';
    }
}
