<?php

namespace App\Services\Images;

use App\Contracts\Services\Images\ViewResult;
use App\Contracts\Storage\Image;
use Illuminate\Http\Request;
use Smorken\Service\Services\BaseService;

class ViewService extends BaseService implements \App\Contracts\Services\Images\ViewService
{
    protected static array $cached = [];

    public function __construct(protected Image $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public static function reset(): void
    {
        self::$cached = [];
    }

    public function getProvider(): Image
    {
        return $this->provider;
    }

    public function view(Request $request, int $id): ViewResult
    {
        $image = $this->findImageVOById($id);

        return new \App\Services\Images\ViewResult($image);
    }

    protected function findImageVOById(int $id): ?\App\Contracts\Models\VO\Image
    {
        $i = self::$cached[$id] ?? false;
        if ($i === false) {
            $model = $this->getProvider()->find($id);
            $i = $this->imageVoFromImageModel($model);
            self::$cached[$id] = $i;
        }

        return $i;
    }

    protected function imageVoFromImageModel(?\App\Contracts\Models\Image $image): \App\Contracts\Models\VO\Image
    {
        return $image ? \App\Models\VO\Image::fromImage($image) : \App\Models\VO\Image::emptyImage();
    }
}
