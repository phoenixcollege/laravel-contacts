<?php

namespace App\Services\Images\Helpers;

use App\Contracts\Services\Images\ViewResult;

class HeadersHelper implements \App\Contracts\Services\Images\HeadersHelper
{
    protected int $cacheAge = 86400;

    public function __construct(protected ViewResult $viewResult)
    {
    }

    public function getExpires(): string
    {
        return gmdate('D, d M Y H:i:s \G\M\T', time() + $this->cacheAge);
    }

    public function getHeaders(): array
    {
        return [
            'Content-Type' => $this->getViewResult()->getMime(),
            'Content-Transfer-Encoding' => 'binary',
            'Content-Disposition' => sprintf('inline; filename="%s"', $this->getViewResult()->getName()),
            'Expires' => $this->getExpires(),
            'Cache-Control' => 'max-age='.$this->cacheAge.', public',
            'Pragma' => 'public',
        ];
    }

    public function getViewResult(): ViewResult
    {
        return $this->viewResult;
    }
}
