<?php

namespace App\Services\Images\Helpers;

use Illuminate\Http\UploadedFile;
use Smorken\Image\Sizer\Contracts\Result;
use Smorken\Image\Sizer\Contracts\Sizer;

trait SizerTrait
{
    protected function convertUploadedFile(UploadedFile $file): Result
    {
        return $this->getSizer()->size($file->get(), ['output' => $this->guessOutPut($file)]);
    }

    protected function getSizer(): Sizer
    {
        return $this->sizer;
    }

    protected function guessOutPut(UploadedFile $file): string
    {
        $mime = $file->getClientMimeType();
        $parts = explode('/', $mime);
        $type = $parts[1] ?? 'png';

        return match ($type) {
            'jpg', 'jpeg' => 'jpg',
            'gif' => 'gif',
            default => 'png',
        };
    }
}
