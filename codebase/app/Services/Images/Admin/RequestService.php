<?php

namespace App\Services\Images\Admin;

use Illuminate\Http\Request;

class RequestService extends \Smorken\Service\Services\RequestService
{
    protected function getNameFromFile(Request $request): string
    {
        $imageFile = $request->file('image');

        return $imageFile?->getClientOriginalName() ?? '';
    }

    protected function modifyRequest(Request $request): Request
    {
        $name = $request->input('name');
        if (! $name) {
            $request = $request->merge(['name' => $this->getNameFromFile($request)]);
        }

        return $request;
    }
}
