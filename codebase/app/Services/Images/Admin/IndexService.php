<?php

namespace App\Services\Images\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \App\Contracts\Services\Images\Admin\IndexService
{
}
