<?php

namespace App\Services\Images\Admin;

use App\Services\Images\Helpers\SizerTrait;
use Illuminate\Http\Request;
use Smorken\Image\Sizer\Contracts\Sizer;
use Smorken\Service\Services\CreateByStorageProviderService;
use Smorken\Storage\Contracts\Base as StorageProvider;

class CreateService extends CreateByStorageProviderService
{
    use SizerTrait;

    public function __construct(protected Sizer $sizer, StorageProvider $provider, array $services = [])
    {
        parent::__construct($provider, $services);
    }

    public function getAttributesFromRequest(Request $request): array
    {
        $attributes = $request->except(['_token']);
        $result = $this->convertUploadedFile($request->file('image'));
        $attributes['image'] = $result->data;
        $attributes['type'] = $result->mime;
        $attributes['size'] = $result->size;

        return $attributes;
    }
}
