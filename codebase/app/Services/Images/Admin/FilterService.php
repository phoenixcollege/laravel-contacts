<?php

namespace App\Services\Images\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        $filter = new \Smorken\Support\Filter([
            'skipImage' => true,
        ]);
        $filter->hide(['skipImage']);

        return $filter;
    }
}
