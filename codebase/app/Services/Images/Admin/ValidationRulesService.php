<?php

namespace App\Services\Images\Admin;

use Illuminate\Http\Request;
use Smorken\Service\Services\ValidationRulesByStorageProviderService;

class ValidationRulesService extends ValidationRulesByStorageProviderService
{
    protected function modifyRulesForUpdate(array $rules, ?Request $request): array
    {
        $rules['name'] = 'required|max:64';
        unset($rules['image']);

        return $rules;
    }
}
