<?php

namespace App\Services\Images\Admin;

use Illuminate\Http\Request;
use Smorken\Image\Sizer\Contracts\Sizer;
use Smorken\Service\Services\UpdateByStorageProviderService;
use Smorken\Storage\Contracts\Base as StorageProvider;

class UpdateService extends UpdateByStorageProviderService
{
    public function __construct(protected Sizer $sizer, StorageProvider $provider, array $services = [])
    {
        parent::__construct($provider, $services);
    }

    public function getAttributesFromRequest(Request $request): array
    {
        return $request->only(['name']);
    }
}
