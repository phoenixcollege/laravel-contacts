<?php

namespace App\Services;

use App\Contracts\Models\Location;
use App\Contracts\Services\Access\UserAccessService;
use Illuminate\Auth\Access\Response;
use Illuminate\Contracts\Auth\Authenticatable;
use Smorken\Service\Services\GatePoliciesService;

class UserAccessPoliciesService extends GatePoliciesService
{
    public function getUserAccessService(): UserAccessService
    {
        return $this->getService(UserAccessService::class);
    }

    protected function getLocationId(mixed $model): ?int
    {
        if ($model) {
            if ($model instanceof Location) {
                return $model->id;
            }

            return $model->location_id;
        }

        return null;
    }

    protected function isUserAllowedToCreate(Authenticatable $user, array $attributes = null): Response
    {
        $typeId = $attributes['location_id'] ?? null;

        return $this->locationIdIsAllowed($user, $typeId);
    }

    protected function isUserAllowedToDestroy(Authenticatable $user, mixed $model): Response
    {
        return $this->locationIdIsAllowed($user, $this->getLocationId($model));
    }

    protected function isUserAllowedToUpdate(Authenticatable $user, mixed $model): Response
    {
        return $this->locationIdIsAllowed($user, $this->getLocationId($model));
    }

    protected function isUserAllowedToView(Authenticatable $user, mixed $model): Response
    {
        return $this->locationIdIsAllowed($user, $this->getLocationId($model));
    }

    protected function isUserAllowedToViewAny(Authenticatable $user): Response
    {
        $allowed = $this->getUserAccessService()->getUserLocationsByUserId($user->getAuthIdentifier());
        if ($allowed->collection->count() > 0) {
            return $this->allow();
        }

        return $this->deny('You do not have any location assignments.');
    }

    protected function locationIdIsAllowed(Authenticatable $user, ?int $locationId): Response
    {
        $denyMessage = "You do not have access to type [$locationId].";
        if (! $locationId) {
            return $this->deny('No Location ID found.');
        }
        $allowed = $this->getUserAccessService()->findLocationByUserId($user->getAuthIdentifier(), $locationId, false);

        return $allowed->result ? $this->allow() : $this->deny($denyMessage);
    }
}
