<?php

namespace App\Services\Extensions;

trait LocalCacheTrait
{
    protected array $cached = [];

    protected function fromCache(string $key, mixed $default = null): mixed
    {
        return $this->cached[$key] ?? $default;
    }

    protected function setCache(string $key, mixed $value): void
    {
        $this->cached[$key] = $value;
    }
}
