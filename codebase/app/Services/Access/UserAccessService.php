<?php

namespace App\Services\Access;

use App\Contracts\Models\Location;
use App\Contracts\Storage\Access;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Smorken\Service\Contracts\Services\VO\CollectionResult;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\BaseService;

class UserAccessService extends BaseService implements \App\Contracts\Services\Access\UserAccessService
{
    public function __construct(protected Access $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function findLocation(Request $request, int $locationId, bool $shouldThrow = true): ModelResult
    {
        return $this->findLocationByUserId($this->getUserIdFromRequest($request), $locationId, $shouldThrow);
    }

    public function findLocationByUserId(int|string $userId, int $locationId, bool $shouldThrow = true): ModelResult
    {
        $locations = $this->getUserLocationsByUserId($userId)->collection;
        $location = $this->findLocationFromAllowed($locationId, $locations, $shouldThrow);

        return new \Smorken\Service\Services\VO\ModelResult($location, $location?->id, (bool) $location);
    }

    public function getProvider(): Access
    {
        return $this->provider;
    }

    public function getUserLocations(Request $request): CollectionResult
    {
        return $this->getUserLocationsByUserId($this->getUserIdFromRequest($request));
    }

    public function getUserLocationsByUserId(int|string $userId): CollectionResult
    {
        $accesses = $this->getProvider()->getAccessByUserId($userId);
        $locations = $this->getLocationsFromAccesses($accesses);

        return new \Smorken\Service\Services\VO\CollectionResult($locations);
    }

    protected function findLocationFromAllowed(int $locationId, Collection $locations, bool $shouldThrow): ?Location
    {
        /** @var Location $location */
        foreach ($locations as $location) {
            if ($location->id === $locationId) {
                return $location;
            }
        }
        if ($shouldThrow) {
            throw new AuthorizationException("You do not have access to the requested location [$locationId].");
        }

        return null;
    }

    protected function getLocationsFromAccesses(Collection $accesses): Collection
    {
        return $accesses->map(function (\App\Contracts\Models\Access $access) {
            return $access->location;
        });
    }

    protected function getUserIdFromRequest(Request $request): string|int
    {
        return $request->user()->getAuthIdentifier();
    }
}
