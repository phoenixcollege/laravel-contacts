<?php

namespace App\Services\Locations\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \App\Contracts\Services\Locations\Admin\IndexService
{
}
