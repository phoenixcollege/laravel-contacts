<?php

namespace App\Services\Locations;

use App\Contracts\Storage\Location;
use Smorken\Service\Contracts\Services\VO\CollectionResult;
use Smorken\Service\Services\BaseService;

class CollectionService extends BaseService implements \App\Contracts\Services\Locations\CollectionService
{
    public function __construct(protected Location $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function active(): CollectionResult
    {
        return new \Smorken\Service\Services\VO\CollectionResult(
            $this->getProvider()->active()
        );
    }

    public function getProvider(): Location
    {
        return $this->provider;
    }
}
