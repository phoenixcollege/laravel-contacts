<?php

namespace App\Services\Users\Admin;

use App\Contracts\Models\User;
use Smorken\Service\Services\VO\VOResult;

class AccessResult extends VOResult implements \App\Contracts\Services\Users\Admin\AccessResult
{
    public function __construct(
        public User $user,
        public bool $updated
    ) {
    }
}
