<?php

namespace App\Services\Users\Admin;

use App\Contracts\Services\Users\Admin\AccessService;
use App\Contracts\Services\Users\Admin\IndexService;
use Smorken\Service\Services\Factory;

class ServicesFactory extends Factory implements \App\Contracts\Services\Users\Admin\ServicesFactory
{
    protected array $services = [
        \App\Contracts\Services\Users\Admin\AccessService::class => null,
        IndexService::class => null,
    ];

    public function getAccessService(): AccessService
    {
        return $this->getService(AccessService::class);
    }

    public function getIndexService(): IndexService
    {
        return $this->getService(IndexService::class);
    }
}
