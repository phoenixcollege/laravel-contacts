<?php

namespace App\Services\Users\Admin;

use App\Contracts\Services\Users\Admin\AccessResult;
use App\Contracts\Storage\User;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\BaseService;
use Smorken\Service\Services\Traits\HasFilterServiceTrait;

class AccessService extends BaseService implements \App\Contracts\Services\Users\Admin\AccessService
{
    use HasFilterServiceTrait;

    protected string $voClass = \App\Services\Users\Admin\AccessResult::class;

    public function __construct(
        protected User $provider,
        array $services = []
    ) {
        parent::__construct($services);
    }

    public function assignmentsFromArray(int|string $userId, array $locations): AccessResult
    {
        $userResult = $this->findUser($userId);
        $updated = $this->getProvider()->handleLocationAssignments($userResult->model, $locations);

        return $this->newVO([
            'user' => $userResult->model,
            'updated' => $updated,
        ]);
    }

    public function assignmentsFromRequest(Request $request, int|string $userId): AccessResult
    {
        $types = $request->input('location', []);

        return $this->assignmentsFromArray($userId, $types);
    }

    public function findUser(int|string $userId): ModelResult
    {
        $user = $this->getProvider()->findOrFail($userId);

        return new \Smorken\Service\Services\VO\ModelResult($user, $userId, true);
    }

    public function getProvider(): User
    {
        return $this->provider;
    }
}
