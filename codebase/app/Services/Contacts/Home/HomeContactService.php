<?php

namespace App\Services\Contacts\Home;

use App\Contracts\Services\Access\UserAccessService;
use App\Contracts\Services\Contacts\Home\IpService;
use App\Contracts\Services\Transactions\RetrieveService;
use App\Contracts\Storage\Contact;
use App\Services\Extensions\LocalCacheTrait;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Smorken\IpAuth\Contracts\Models\Active;
use Smorken\IpAuth\Contracts\Services\User\ActiveUserService;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\BaseService;

class HomeContactService extends BaseService implements \App\Contracts\Services\Contacts\Home\HomeContactService
{
    use LocalCacheTrait;

    public function __construct(protected Contact $provider, array $services = [])
    {
        parent::__construct($services);
    }

    public function add(Request $request, int $transactionId): ModelResult
    {
        /** @var \App\Contracts\Models\Transaction $transaction */
        $transaction = $this->getTransactionRetrieveService()->findById($transactionId)->model;
        $this->verifyUserAccessToLocationId($request, $transaction->category->location_id);
        $contact = $this->getProvider()->add($this->getActiveUser($request)->user_id, $transactionId);

        return new \Smorken\Service\Services\VO\ModelResult(
            $contact,
            $contact?->id,
            (bool) $contact
        );
    }

    public function deleteLast(Request $request): ModelResult
    {
        /** @var \App\Models\Eloquent\Ip $ip */
        $ip = $this->getIpService()->findIp($request)->model;

        return $this->deleteLastByLocationId($request, $ip->location_id);
    }

    public function deleteLastByLocationId(Request $request, int $locationId): ModelResult
    {
        $this->verifyUserAccessToLocationId($request, $locationId);
        $last = $this->getProvider()->lastByLocationId($locationId);
        $deleted = ! $last || $this->getProvider()->delete($last);

        return new \Smorken\Service\Services\VO\ModelResult(
            $last,
            $last?->id,
            $deleted
        );
    }

    public function findLast(Request $request): ModelResult
    {
        /** @var \App\Models\Eloquent\Ip $ip */
        $ip = $this->getIpService()->findIp($request)->model;

        return $this->findLastByLocationId($request, $ip->location_id);
    }

    public function findLastByLocationId(Request $request, int $locationId): ModelResult
    {
        $this->verifyUserAccessToLocationId($request, $locationId);
        $last = $this->getProvider()->lastByLocationId($locationId);

        return new \Smorken\Service\Services\VO\ModelResult(
            $last,
            $last?->id,
            (bool) $last
        );
    }

    public function getActiveUserService(): ActiveUserService
    {
        return $this->getService(ActiveUserService::class);
    }

    public function getIpService(): IpService
    {
        return $this->getService(IpService::class);
    }

    public function getProvider(): Contact
    {
        return $this->provider;
    }

    public function getTransactionRetrieveService(): RetrieveService
    {
        return $this->getService(RetrieveService::class);
    }

    public function getUserAccessService(): UserAccessService
    {
        return $this->getService(UserAccessService::class);
    }

    protected function getActiveUser(Request $request): ?Active
    {
        $cached = $this->fromCache('activeUser');
        if ($cached) {
            return $cached;
        }
        $activeUser = $this->getActiveUserService()->findActiveUser($request)->model;
        $this->setCache('activeUser', $activeUser);

        return $activeUser;
    }

    protected function verifyUserAccessToLocationId(Request $request, int $locationId): void
    {
        $cached = $this->fromCache('activeUserAccess');
        if ($cached === true) {
            return;
        }
        $activeUser = $this->getActiveUser($request);
        if ($activeUser && $this->getUserAccessService()
            ->findLocationByUserId($activeUser->user_id, $locationId)->result) {
            $this->setCache('activeUserAccess', true);

            return;
        }
        throw new AuthorizationException('The current user does not have access to the requested location.');
    }
}
