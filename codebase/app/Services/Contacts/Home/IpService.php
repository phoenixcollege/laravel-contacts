<?php

namespace App\Services\Contacts\Home;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Smorken\IpAuth\Contracts\Services\Ip\ValidIpService;
use Smorken\Service\Contracts\Services\VO\ModelResult;
use Smorken\Service\Services\BaseService;

class IpService extends BaseService implements \App\Contracts\Services\Contacts\Home\IpService
{
    public function findIp(Request $request): ModelResult
    {
        $result = $this->getValidIpService()->findValid($request);
        if (! $result->result) {
            throw new AuthorizationException('No valid IP/location found.');
        }

        return $result;
    }

    public function getValidIpService(): ValidIpService
    {
        return $this->getService(ValidIpService::class);
    }
}
