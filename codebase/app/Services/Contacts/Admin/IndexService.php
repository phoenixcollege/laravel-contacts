<?php

namespace App\Services\Contacts\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \App\Contracts\Services\Contacts\Admin\IndexService
{
}
