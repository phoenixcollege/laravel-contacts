<?php

namespace App\Services\Contacts\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        return \Smorken\Support\Filter::fromRequest($request, ['f_locationId', 'f_startAfter', 'f_startBefore']);
    }
}
