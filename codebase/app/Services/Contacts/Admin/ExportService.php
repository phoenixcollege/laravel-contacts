<?php

namespace App\Services\Contacts\Admin;

use App\Contracts\Models\Contact;
use Smorken\Export\Contracts\Export;
use Smorken\Service\Services\ExportByStorageProviderService;

class ExportService extends ExportByStorageProviderService implements \App\Contracts\Services\Contacts\Admin\ExportService
{
    protected function getHeaderCallback(...$params): ?callable
    {
        return function (Export $export, array $headers, ?Contact $first): array {
            return [
                'id',
                'user_id',
                'user_name',
                'transaction_id',
                'transaction',
                'category_id',
                'category',
                'location_id',
                'location',
                'created',
                'updated',
            ];
        };
    }

    protected function getRowCallback(...$params): ?callable
    {
        return function (Export $export, array $data, Contact $model): array {
            $user = $model->user;
            $transaction = $model->transaction;
            $category = $transaction?->category;
            $location = $category?->location;

            return [
                [
                    'id' => $model->id,
                    'user_id' => $model->user_id,
                    'user_name' => $user?->name(),
                    'transaction_id' => $model->transaction_id,
                    'transaction' => $transaction?->transaction,
                    'category_id' => $transaction?->category_id,
                    'category' => $category?->category,
                    'location_id' => $category?->location_id,
                    'location' => $location?->name,
                    'created' => $model->created_at,
                    'updated' => $model->updated_at,
                ],
            ];
        };
    }
}
