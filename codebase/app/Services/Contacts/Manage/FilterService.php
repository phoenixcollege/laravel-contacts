<?php

namespace App\Services\Contacts\Manage;

use App\Contracts\Services\Access\UserAccessService;
use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService implements \App\Contracts\Services\Contacts\Manage\FilterService
{
    public function getUserAccessService(): UserAccessService
    {
        return $this->getService(UserAccessService::class);
    }

    protected function createFilterFromRequest(Request $request): Filter
    {
        $filter = \Smorken\Support\Filter::fromRequest($request, ['f_locationId', 'f_startAfter', 'f_startBefore']);
        $this->ensureLocationId($request, $filter);

        return $filter;
    }

    protected function ensureLocationId(Request $request, Filter $filter): void
    {
        if ($filter->f_locationId && $this->getUserAccessService()
            ->findLocation($request, $filter->f_locationId, false)->result) {
            return;
        }
        $filter->f_locationId = $this->getUserAccessService()
            ->getUserLocations($request)->collection->first()?->id ?? 0;
    }
}
