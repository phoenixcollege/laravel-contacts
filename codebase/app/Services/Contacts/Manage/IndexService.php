<?php

namespace App\Services\Contacts\Manage;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \App\Contracts\Services\Contacts\Manage\IndexService
{
}
