<?php

namespace App\Services\Contacts\Manage;

use App\Contracts\Models\Contact;

class UserAccessPoliciesService extends \App\Services\UserAccessPoliciesService
{
    protected string $modelClass = Contact::class;
}
