<?php

namespace App\Services\Transactions;

use Smorken\Service\Services\RetrieveByStorageProviderService;

class RetrieveService extends RetrieveByStorageProviderService implements \App\Contracts\Services\Transactions\RetrieveService
{
}
