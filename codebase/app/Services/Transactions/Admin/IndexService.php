<?php

namespace App\Services\Transactions\Admin;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \App\Contracts\Services\Transactions\Admin\IndexService
{
}
