<?php

namespace App\Services\Transactions\Admin;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{
    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'f_categoryId' => $request->input('f_categoryId'),
            'f_active' => $request->input('f_active'),
        ]);
    }
}
