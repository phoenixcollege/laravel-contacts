<?php

namespace App\Storage\Eloquent;

use App\Validation\RuleProviders\AccessRules;
use Illuminate\Support\Collection;

class Access extends Base implements \App\Contracts\Storage\Access
{
    public function accessByUserIdAndLocationId(int|string $userId, int $locationId): ?\App\Contracts\Models\Access
    {
        return $this->getCacheAssist()->remember([$userId, $locationId],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime, function () use ($userId, $locationId) {
                return $this->getModel()
                    ->newQuery()
                    ->userIdIs($userId)
                    ->locationIdIs($locationId)
                    ->first();
            });
    }

    public function getAccessByUserId(int|string $userId): Collection
    {
        return $this->getCacheAssist()->remember([$userId],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime, function () use ($userId) {
                return $this->getModel()
                    ->newQuery()
                    ->userIdIs($userId)
                    ->hasActiveLocation()
                    ->defaultWiths()
                    ->defaultOrder()
                    ->get();
            });
    }

    public function validationRules(array $override = []): array
    {
        return AccessRules::rules($override);
    }
}
