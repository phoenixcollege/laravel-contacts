<?php

namespace App\Storage\Eloquent;

use App\Validation\RuleProviders\LocationRules;
use Illuminate\Support\Collection;

class Location extends Base implements \App\Contracts\Storage\Location
{
    public function active(): Collection
    {
        return $this->getCacheAssist()
            ->remember(['active'], $this->getCacheAssist()->getCacheOptions()->defaultCacheTime, function () {
                return $this->getModel()
                    ->newQuery()
                    ->isActive()
                    ->defaultOrder()
                    ->get();
            });
    }

    public function validationRules(array $override = []): array
    {
        return LocationRules::rules($override);
    }
}
