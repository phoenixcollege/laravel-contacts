<?php

namespace App\Storage\Eloquent;

use App\Validation\RuleProviders\ContactRules;
use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;

class Contact extends Base implements \App\Contracts\Storage\Contact
{
    public function add(int|string $userId, int $transactionId): \App\Contracts\Models\Contact
    {
        return $this->create(['user_id' => $userId, 'transaction_id' => $transactionId]);
    }

    public function deleteLastByLocationId(int $locationId): bool
    {
        return $this->getModel()
            ->newQuery()
            ->hasLocationId($locationId)
            ->defaultOrder()
            ->limitOne()
            ->delete();
    }

    public function lastByLocationId(int $locationId): ?\App\Contracts\Models\Contact
    {
        return $this->getModel()
            ->newQuery()
            ->hasLocationId($locationId)
            ->defaultOrder()
            ->first();
    }

    public function validationRules(array $override = []): array
    {
        return ContactRules::rules($override);
    }

    protected function filterEndDate(Builder $query, $value): Builder
    {
        if (strlen($value)) {
            $date = Carbon::parse($value);

            return $query->createdBefore($date->format('Y-m-d'));
        }

        return $query;
    }

    protected function filterLocationId(Builder $query, $value): Builder
    {
        if (strlen($value)) {
            return $query->hasLocationId($value);
        }

        return $query;
    }

    protected function filterStartDate(Builder $query, $value): Builder
    {
        if (strlen($value)) {
            $date = Carbon::parse($value);

            return $query->createdAfter($date->format('Y-m-d'));
        }

        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_locationId' => 'filterLocationId',
            'f_startAfter' => 'filterStartDate',
            'f_startBefore' => 'filterEndDate',
        ];
    }
}
