<?php

namespace App\Storage\Eloquent;

use App\Validation\RuleProviders\ImageRules;
use Illuminate\Contracts\Database\Eloquent\Builder;

class Image extends Base implements \App\Contracts\Storage\Image
{
    public function updateName(int $id, string $name): \App\Contracts\Models\Image
    {
        return $this->getModel()->idIs($id)->update(['name' => $name]);
    }

    public function validationRules(array $override = []): array
    {
        return ImageRules::rules($override);
    }

    protected function filterSkipImage(Builder $query, $value): Builder
    {
        if ($value === true) {
            return $query->skipImage();
        }

        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'skipImage' => 'filterSkipImage',
        ];
    }
}
