<?php

namespace App\Storage\Eloquent;

use App\Validation\RuleProviders\CategoryRules;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class Category extends Base implements \App\Contracts\Storage\Category
{
    public function active(): Collection
    {
        return $this->getCacheAssist()
            ->remember(['active'], $this->getCacheAssist()->getCacheOptions()->defaultCacheTime, function () {
                return $this->getModel()
                    ->newQuery()
                    ->activeIs(1)
                    ->defaultOrder()
                    ->defaultWiths()
                    ->get();
            });
    }

    public function getByLocationId(int $locationId): Collection
    {
        return $this->getCacheAssist()->remember(['location', $locationId],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime, function () use ($locationId) {
                return $this->getModel()
                    ->newQuery()
                    ->activeIs(1)
                    ->locationIdIs($locationId)
                    ->with('transactions')
                    ->with(
                        [
                            'transactions.image' => function ($sq) {
                                $sq->skipImage();
                            },
                        ]
                    )
                    ->defaultOrder()
                    ->defaultWiths()
                    ->get();
            });
    }

    public function validationRules(array $override = []): array
    {
        return CategoryRules::rules($override);
    }

    protected function filterActive(Builder $q, $v): Builder
    {
        if (strlen($v)) {
            $q->activeIs($v);
        }

        return $q;
    }

    protected function filterLocationId(Builder $q, $v): Builder
    {
        if (strlen($v)) {
            $q->locationIdIs($v);
        }

        return $q;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_locationId' => 'filterLocationId',
            'f_active' => 'filterActive',
        ];
    }
}
