<?php

namespace App\Storage\Eloquent;

use App\Validation\RuleProviders\TransactionRules;
use Illuminate\Contracts\Database\Eloquent\Builder;

class Transaction extends Base implements \App\Contracts\Storage\Transaction
{
    public function validationRules(array $override = []): array
    {
        return TransactionRules::rules($override);
    }

    protected function filterActive(Builder $query, $value): Builder
    {
        if (strlen($value)) {
            return $query->activeIs($value);
        }

        return $query;
    }

    protected function filterCategoryIdIs(Builder $query, $value): Builder
    {
        if (strlen($value)) {
            return $query->categoryIdIs($value);
        }

        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_categoryId' => 'filterCategoryIdIs',
            'f_active' => 'filterActive',
        ];
    }
}
