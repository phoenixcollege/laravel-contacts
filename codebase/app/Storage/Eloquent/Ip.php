<?php

namespace App\Storage\Eloquent;

use App\Validation\RuleProviders\IpRules;

class Ip extends \Smorken\IpAuth\Storage\Eloquent\Ip
{
    public function setValid(array $data): ?\Smorken\IpAuth\Contracts\Models\Ip
    {
        $this->getCacheAssist()->forget(['valid', $data['ip'] ?? 'unk']);

        return $this->getModel()
            ->updateOrCreate(
                ['ip' => $data['ip'] ?? null],
                [
                    'user_id' => $data['user_id'] ?? null,
                    'valid_until' => $data['valid_until'] ?? null,
                    'location_id' => $data['location_id'] ?? 0,
                ]
            );
    }

    public function validationRules(array $override = []): array
    {
        return IpRules::rules($override);
    }
}
