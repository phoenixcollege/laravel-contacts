<?php

namespace App\Storage\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class User extends \Smorken\IpAuth\Storage\Eloquent\User implements \App\Contracts\Storage\User
{
    public function get(...$params): Collection
    {
        $locationId = $params[0] ?? null;

        return $this->getCacheAssist()->remember([$locationId],
            $this->getCacheAssist()->getCacheOptions()->defaultCacheTime, function () use ($locationId) {
                return $this->getModel()
                    ->newQuery()
                    ->accessHasLocationId($locationId)
                    ->orderDefault()
                    ->get();
            });
    }

    public function handleLocationAssignments(\App\Contracts\Models\User $user, array $locations): bool
    {
        if (empty($locations)) {
            return (bool) $this->removeLocations($user);
        }

        return (bool) $this->syncLocations($user, $locations);
    }

    protected function filterFirstName(Builder $query, $value): Builder
    {
        if (strlen($value)) {
            $query = $query->firstNameLike($value);
        }

        return $query;
    }

    protected function filterLastName(Builder $query, $value): Builder
    {
        if (strlen($value)) {
            $query = $query->lastNameLike($value);
        }

        return $query;
    }

    protected function filterLocationId(Builder $query, $value): Builder
    {
        if (strlen($value)) {
            $query = $query->accessHasLocationId($value);
        }

        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_lastName' => 'filterLastName',
            'f_firstName' => 'filterFirstName',
            'f_locationId' => 'filterLocationId',
        ];
    }

    protected function removeLocations(\App\Contracts\Models\User $user): int
    {
        return $user->locations()
            ->detach();
    }

    protected function syncLocations(\App\Contracts\Models\User $user, array $locations): array
    {
        $ids = [];
        foreach ($locations as $id => $set) {
            if ($id && $set) {
                $ids[] = $id;
            }
        }

        return $user->locations()
            ->sync($ids);
    }
}
