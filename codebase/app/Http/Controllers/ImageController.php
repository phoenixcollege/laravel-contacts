<?php

namespace App\Http\Controllers;

use App\Contracts\Services\Images\ViewService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ImageController extends \Illuminate\Routing\Controller
{
    public function __construct(protected ViewService $viewService)
    {
    }

    public function __invoke(Request $request, int $id): Response
    {
        $result = $this->viewService->view($request, $id);

        return $result->toResponse();
    }
}
