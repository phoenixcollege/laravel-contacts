<?php

namespace App\Http\Controllers\Admin\Location;

use App\Contracts\Services\Locations\Admin\CrudServices;
use App\Contracts\Services\Locations\Admin\IndexService;
use Smorken\Controller\View\WithService\CrudController;

class Controller extends CrudController
{
    protected string $baseView = 'admin.location';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
