<?php

namespace App\Http\Controllers\Admin\Report;

use App\Contracts\Services\Contacts\Admin\ExportService;
use App\Contracts\Services\Contacts\Admin\IndexService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Controller extends \Smorken\Controller\View\Controller
{
    protected string $baseView = 'admin.report';

    public function __construct(
        protected IndexService $indexService,
        protected ExportService $exportService
    ) {
        parent::__construct();
    }

    public function export(Request $request): Response
    {
        $result = $this->exportService->export($request);

        return $result->exporter->output('contacts');
    }

    public function index(Request $request): View
    {
        $result = $this->indexService->getByRequest($request);

        return $this->makeView($this->getViewName('index'))
            ->with('filter', $result->filter)
            ->with('models', $result->models);
    }
}
