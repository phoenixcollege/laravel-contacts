<?php

namespace App\Http\Controllers\Admin\Image;

use App\Contracts\Services\Images\Admin\CrudServices;
use App\Contracts\Services\Images\Admin\IndexService;
use Smorken\Controller\View\WithService\CrudController;

class Controller extends CrudController
{
    protected string $baseView = 'admin.image';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
