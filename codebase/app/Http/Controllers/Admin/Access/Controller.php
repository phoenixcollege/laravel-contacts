<?php

namespace App\Http\Controllers\Admin\Access;

use App\Contracts\Services\Users\Admin\ServicesFactory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Service\Services\VO\RedirectActionResult;

class Controller extends \Smorken\Controller\View\Controller
{
    protected string $baseView = 'admin.access';

    public function __construct(
        protected ServicesFactory $factory
    ) {
        parent::__construct();
    }

    public function assign(Request $request, int|string $id): View
    {
        $userResult = $this->factory->getAccessService()->findUser($id);
        $filter = $this->factory->getIndexService()->getFilterService()->getFilterFromRequest($request);

        return $this->makeView($this->getViewName('assign'))
            ->with('user', $userResult->model)
            ->with('filter', $filter);
    }

    public function doAssign(Request $request, int|string $id): RedirectResponse
    {
        $result = $this->factory->getAccessService()->assignmentsFromRequest($request, $id);
        $filter = $this->factory->getIndexService()->getFilterService()->getFilterFromRequest($request);
        if ($result->updated) {
            $request->session()->flash('flash:success', "Assignments modified for [$id].");
        }

        return (new RedirectActionResult($this->actionArray('index'), $filter->toArray()))->redirect();
    }

    public function index(Request $request): View
    {
        $result = $this->factory->getIndexService()->getByRequest($request);

        return $this->makeView($this->getViewName('index'))
            ->with('models', $result->models)
            ->with('filter', $result->filter);
    }

    public function remove(Request $request, int|string $id): RedirectResponse
    {
        $request = $request->merge(['location' => []]);

        return $this->doAssign($request, $id);
    }
}
