<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Contracts\Services\Transactions\Admin\CrudServices;
use App\Contracts\Services\Transactions\Admin\IndexService;
use Smorken\Controller\View\WithService\CrudController;

class Controller extends CrudController
{
    protected string $baseView = 'admin.transaction';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
