<?php

namespace App\Http\Controllers\Admin\Category;

use App\Contracts\Services\Categories\Admin\CrudServices;
use App\Contracts\Services\Categories\Admin\IndexService;
use Smorken\Controller\View\WithService\CrudController;

class Controller extends CrudController
{
    protected string $baseView = 'admin.category';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
