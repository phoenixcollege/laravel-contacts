<?php

namespace App\Http\Controllers;

use App\Contracts\Services\Categories\Home\IndexService;
use App\Contracts\Services\Contacts\Home\HomeContactService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Smorken\Service\Services\VO\RedirectActionResult;

class HomeController extends \Smorken\Controller\View\Controller
{
    protected string $baseView = 'home';

    public function __construct(protected HomeContactService $contactService, protected IndexService $indexService)
    {
        parent::__construct();
    }

    public function add(Request $request, int $transactionId): RedirectResponse|JsonResponse
    {
        $result = $this->contactService->add($request, $transactionId);
        if (! $result->result) {
            $request->session()->flash('flash:danger', 'Unable to add a new contact!');
        }

        return $this->toJsonLastOrRedirect($request);
    }

    public function deleteLast(Request $request): RedirectResponse|JsonResponse
    {
        $result = $this->contactService->deleteLast($request);
        if (! $result->result) {
            $request->session()->flash('flash:danger', 'Unable to delete the last contact.');
        }

        return $this->toJsonLastOrRedirect($request);
    }

    public function index(Request $request): View
    {
        $result = $this->indexService->getByRequest($request);

        return $this->makeView($this->getViewName('index'))
            ->with('ip', $result->ip)
            ->with('models', $result->models)
            ->with('filter', $result->filter);
    }

    protected function getParams(Request $request): array
    {
        return $this->indexService->getFilterService()->getFilterFromRequest($request)->toArray();
    }

    protected function toJsonLastOrRedirect(Request $request): RedirectResponse|JsonResponse
    {
        if ($request->wantsJson()) {
            return Response::json([
                'content' => $this->makeView($this->getViewName('_last_contact'))->render(),
            ]);
        }

        return (new RedirectActionResult($this->actionArray('index'), $this->getParams($request)))->redirect();
    }
}
