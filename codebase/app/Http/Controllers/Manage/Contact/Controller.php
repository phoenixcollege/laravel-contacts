<?php

namespace App\Http\Controllers\Manage\Contact;

use App\Contracts\Services\Contacts\Manage\CrudServices;
use App\Contracts\Services\Contacts\Manage\IndexService;
use Smorken\Controller\View\WithService\CrudController;

class Controller extends CrudController
{
    protected string $baseView = 'manage.contact';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
