<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table(
            'ips',
            function (Blueprint $t) {
                $t->integer('location_id')
                    ->unsigned()
                    ->default(0);

                $t->index('location_id', 'ips_location_id_ndx');
            }
        );
    }
};
