<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        $tables = [
            'accesses',
            'categories',
            'contacts',
            'images',
            'locations',
            'transactions',
        ];
        foreach ($tables as $table) {
            Schema::dropIfExists($table);
        }
    }

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(
            'accesses',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('user_id', 32);
                $t->integer('location_id')
                    ->unsigned();
                $t->boolean('active')
                    ->default(true);
                $t->timestamps();

                $t->index('user_id', 'acc_user_id_ndx');
                $t->index('location_id', 'acc_location_id_ndx');
                $t->index('active', 'acc_active_ndx');
            }
        );

        Schema::create(
            'categories',
            function (Blueprint $t) {
                $t->increments('id');
                $t->integer('location_id')
                    ->unsigned();
                $t->string('category', 128)
                    ->nullable();
                $t->boolean('active')
                    ->default(true);
                $t->timestamps();

                $t->index('location_id', 'cat_location_id_ndx');
                $t->index('active', 'cat_active_ndx');
            }
        );

        Schema::create(
            'contacts',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('user_id', 32);
                $t->integer('transaction_id')
                    ->unsigned();
                $t->timestamps();

                $t->index('user_id', 'con_user_id_ndx');
                $t->index('transaction_id', 'con_transaction_id_ndx');
            }
        );

        Schema::create(
            'images',
            function (Blueprint $t) {
                $t->increments('id');
                $t->binary('image');
                $t->integer('size');
                $t->string('name', 64);
                $t->string('type', 15);
                $t->timestamps();
            }
        );

        Schema::create(
            'locations',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('name', 64);
                $t->boolean('active')
                    ->default(true);
                $t->timestamps();

                $t->index('active', 'loc_active_ndx');
            }
        );

        Schema::create(
            'transactions',
            function (Blueprint $t) {
                $t->increments('id');
                $t->string('transaction', 128);
                $t->integer('category_id')
                    ->unsigned();
                $t->integer('image_id')
                    ->unsigned();
                $t->boolean('active')
                    ->default(true);
                $t->timestamps();

                $t->index('category_id', 'tran_category_id_ndx');
                $t->index('active', 'tran_active_ndx');
            }
        );
    }
};
