<?php

namespace Database\Factories\Eloquent;

use App\Models\Eloquent\Ip;

class IpFactory extends \Database\Factories\Smorken\IpAuth\Models\Eloquent\IpFactory
{
    protected $model = Ip::class;

    public function definition(): array
    {
        $parent = parent::definition();
        $parent['location_id'] = $this->faker->randomNumber(2);

        return $parent;
    }
}
