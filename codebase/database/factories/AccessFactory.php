<?php

namespace Database\Factories\Eloquent;

use App\Models\Eloquent\Access;
use Illuminate\Database\Eloquent\Factories\Factory;

class AccessFactory extends Factory
{
    protected $model = Access::class;

    public function definition(): array
    {
        return [
            'location_id' => $this->faker->randomNumber(3),
            'user_id' => $this->faker->randomNumber(2),
            'active' => 1,
        ];
    }
}
