<?php

namespace Database\Factories\Eloquent;

use App\Models\Eloquent\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    protected $model = Category::class;

    public function definition(): array
    {
        return [
            'location_id' => $this->faker->randomNumber(3),
            'category' => $this->faker->words(2, true),
            'active' => 1,
        ];
    }
}
