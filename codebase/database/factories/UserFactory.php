<?php

namespace Database\Factories\Eloquent;

use App\Models\Eloquent\User;
use Database\Factories\Smorken\IpAuth\Models\Eloquent\User\BaseFactory;
use Illuminate\Support\Str;

class UserFactory extends BaseFactory
{
    protected $model = User::class;

    public function definition(): array
    {
        $parent = parent::definition();
        $parent['username'] = Str::random(8);
        $parent['email'] = $this->faker->safeEmail;

        return $parent;
    }
}
