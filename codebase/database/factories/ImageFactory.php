<?php

namespace Database\Factories\Eloquent;

use App\Models\Eloquent\Image;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    protected $model = Image::class;

    public function definition(): array
    {
        $cont = file_get_contents(__DIR__.'/1x1.png');

        return [
            'image' => $cont,
            'size' => strlen($cont),
            'name' => '1x1.png',
            'type' => 'image/png',
        ];
    }
}
