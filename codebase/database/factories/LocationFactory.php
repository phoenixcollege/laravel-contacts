<?php

namespace Database\Factories\Eloquent;

use App\Models\Eloquent\Location;
use Illuminate\Database\Eloquent\Factories\Factory;

class LocationFactory extends Factory
{
    protected $model = Location::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->words(2, true),
            'active' => 1,
        ];
    }
}
