<?php

namespace Database\Factories\Eloquent;

use App\Models\Eloquent\Contact;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactFactory extends Factory
{
    protected $model = Contact::class;

    public function definition(): array
    {
        return [
            'transaction_id' => $this->faker->randomNumber(3),
            'user_id' => $this->faker->randomNumber(2),
        ];
    }
}
