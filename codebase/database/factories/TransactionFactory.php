<?php

namespace Database\Factories\Eloquent;

use App\Models\Eloquent\Transaction;
use Illuminate\Database\Eloquent\Factories\Factory;

class TransactionFactory extends Factory
{
    protected $model = Transaction::class;

    public function definition(): array
    {
        return [
            'category_id' => $this->faker->randomNumber(3),
            'transaction' => $this->faker->words(2, true),
            'image_id' => 0,
            'active' => 1,
        ];
    }
}
