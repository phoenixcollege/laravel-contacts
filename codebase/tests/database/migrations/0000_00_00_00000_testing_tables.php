<?php

return new class extends \Illuminate\Database\Migrations\Migration
{
    use \Tests\App\Traits\Table;

    protected $connection = null;

    protected array $tables = [];

    public function down(): void
    {
        foreach ($this->tables as $t) {
            $this->deleteTableFromModelClass($t);
        }
    }

    public function up(): void
    {
        foreach ($this->tables as $t) {
            $this->createTableFromModelClass($t);
        }
    }
};
