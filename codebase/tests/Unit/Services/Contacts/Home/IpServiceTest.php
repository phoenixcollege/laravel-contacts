<?php

namespace Tests\App\Unit\Services\Contacts\Home;

use App\Contracts\Services\Contacts\Home\IpService;
use App\Models\Eloquent\Ip;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\IpAuth\Contracts\Services\Ip\ValidIpService;
use Smorken\Service\Services\VO\ModelResult;

class IpServiceTest extends TestCase
{
    public function testFindIpWithValidIp(): void
    {
        $sut = $this->getSut();
        $result = new ModelResult((new Ip()), 1, true);
        $request = m::mock(Request::class);
        $request->shouldReceive('ip')->andReturn('1.1.1.1');
        $sut->getValidIpService()->shouldReceive('findValid')
            ->once()
            ->with($request)
            ->andReturn($result);
        $this->assertSame($result, $sut->findIp($request));
    }

    public function testFindIpWithoutValidIpIsException(): void
    {
        $sut = $this->getSut();
        $result = new ModelResult(null, null, false);
        $request = m::mock(Request::class);
        $request->shouldReceive('ip')->andReturn('1.1.1.1');
        $sut->getValidIpService()->shouldReceive('findValid')
            ->once()
            ->with($request)
            ->andReturn($result);
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('No valid IP/location found.');
        $sut->findIp($request);
    }

    protected function getSut(): IpService
    {
        return new \App\Services\Contacts\Home\IpService([
            ValidIpService::class => m::mock(ValidIpService::class),
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
