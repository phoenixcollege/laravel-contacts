<?php

namespace Tests\App\Unit\Services\Contacts\Home;

use App\Contracts\Models\Transaction;
use App\Contracts\Services\Access\UserAccessService;
use App\Contracts\Services\Contacts\Home\HomeContactService;
use App\Contracts\Services\Contacts\Home\IpService;
use App\Contracts\Services\Transactions\RetrieveService;
use App\Contracts\Storage\Contact;
use App\Models\Eloquent\Category;
use App\Models\Eloquent\Ip;
use App\Models\Eloquent\Location;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\IpAuth\Contracts\Services\User\ActiveUserService;
use Smorken\IpAuth\Models\Eloquent\Active;
use Smorken\Service\Services\VO\ModelResult;

class HomeContactServiceTest extends TestCase
{
    public function testAddCanAddContact(): void
    {
        $sut = $this->getSut();
        $request = $this->getRequestMock();
        $transaction = $this->createTransaction(11);
        $sut->getTransactionRetrieveService()->shouldReceive('findById')
            ->once()
            ->with(111)
            ->andReturn(new ModelResult($transaction, $transaction->id, true));
        $sut->getIpService()->shouldReceive('findIp')
            ->never();
        $sut->getActiveUserService()->shouldReceive('findActiveUser')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult((new Active())->forceFill(['user_id' => 1]), 1, true));
        $sut->getUserAccessService()->shouldReceive('findLocationByUserId')
            ->once()
            ->with(1, 11)
            ->andReturn(new ModelResult((new Location())->forceFill(['id' => 11]), 11, true));
        $contact = new \App\Models\Eloquent\Contact();
        $sut->getProvider()->shouldReceive('add')
            ->once()
            ->with(1, 111)
            ->andReturn($contact);
        $result = $sut->add($request, 111);
        $this->assertSame($contact, $result->model);
    }

    public function testDeleteLastCanDeleteLastContact(): void
    {
        $sut = $this->getSut();
        $request = $this->getRequestMock();
        $sut->getIpService()->shouldReceive('findIp')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult((new Ip())->forceFill(['location_id' => 11]), 1, true));
        $sut->getActiveUserService()->shouldReceive('findActiveUser')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult((new Active())->forceFill(['user_id' => 1]), 1, true));
        $sut->getUserAccessService()->shouldReceive('findLocationByUserId')
            ->once()
            ->with(1, 11)
            ->andReturn(new ModelResult((new Location())->forceFill(['id' => 11]), 11, true));
        $contact = new \App\Models\Eloquent\Contact();
        $sut->getProvider()->shouldReceive('lastByLocationId')
            ->once()
            ->with(11)
            ->andReturn($contact);
        $sut->getProvider()->shouldReceive('delete')
            ->once()
            ->with($contact)
            ->andReturn(true);
        $result = $sut->deleteLast($request);
        $this->assertSame($contact, $result->model);
    }

    public function testDeleteLastCanDeleteNoContactIsTrue(): void
    {
        $sut = $this->getSut();
        $request = $this->getRequestMock();
        $sut->getIpService()->shouldReceive('findIp')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult((new Ip())->forceFill(['location_id' => 11]), 1, true));
        $sut->getActiveUserService()->shouldReceive('findActiveUser')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult((new Active())->forceFill(['user_id' => 1]), 1, true));
        $sut->getUserAccessService()->shouldReceive('findLocationByUserId')
            ->once()
            ->with(1, 11)
            ->andReturn(new ModelResult((new Location())->forceFill(['id' => 11]), 11, true));
        $sut->getProvider()->shouldReceive('lastByLocationId')
            ->once()
            ->with(11)
            ->andReturn(null);
        $sut->getProvider()->shouldReceive('delete')
            ->never();
        $result = $sut->deleteLast($request);
        $this->assertTrue($result->result);
    }

    public function testFindLastCanReturnLastContact(): void
    {
        $sut = $this->getSut();
        $request = $this->getRequestMock();
        $sut->getIpService()->shouldReceive('findIp')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult((new Ip())->forceFill(['location_id' => 11]), 1, true));
        $sut->getActiveUserService()->shouldReceive('findActiveUser')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult((new Active())->forceFill(['user_id' => 1]), 1, true));
        $sut->getUserAccessService()->shouldReceive('findLocationByUserId')
            ->once()
            ->with(1, 11)
            ->andReturn(new ModelResult((new Location())->forceFill(['id' => 11]), 11, true));
        $contact = new \App\Models\Eloquent\Contact();
        $sut->getProvider()->shouldReceive('lastByLocationId')
            ->once()
            ->with(11)
            ->andReturn($contact);
        $result = $sut->findLast($request);
        $this->assertSame($contact, $result->model);
    }

    public function testFindLastWithoutActiveUserIsException(): void
    {
        $sut = $this->getSut();
        $request = $this->getRequestMock();
        $sut->getIpService()->shouldReceive('findIp')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult((new Ip())->forceFill(['location_id' => 11]), 1, true));
        $sut->getActiveUserService()->shouldReceive('findActiveUser')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult(null, null, false));
        $sut->getUserAccessService()->shouldReceive('findLocationByUserId')
            ->never();
        $sut->getProvider()->shouldReceive('lastByLocationId')
            ->never();
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('The current user does not have access to the requested location.');
        $result = $sut->findLast($request);
    }

    public function testFindLastWithoutUserAccessToLocationIsException(): void
    {
        $sut = $this->getSut();
        $request = $this->getRequestMock();
        $sut->getIpService()->shouldReceive('findIp')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult((new Ip())->forceFill(['location_id' => 11]), 1, true));
        $sut->getActiveUserService()->shouldReceive('findActiveUser')
            ->once()
            ->with($request)
            ->andReturn(new ModelResult((new Active())->forceFill(['user_id' => 1]), 1, true));
        $sut->getUserAccessService()->shouldReceive('findLocationByUserId')
            ->once()
            ->with(1, 11)
            ->andReturn(new ModelResult(null, null, false));
        $sut->getProvider()->shouldReceive('lastByLocationId')
            ->never();
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('The current user does not have access to the requested location.');
        $result = $sut->findLast($request);
    }

    protected function createTransaction(int $locationId, int $transactionId = 111): Transaction
    {
        $category = (new Category())->forceFill(['id' => 1111, 'location_id' => $locationId]);
        $transaction = (new \App\Models\Eloquent\Transaction())->forceFill([
            'id' => $transactionId, 'category_id' => $category->id,
        ]);
        $transaction->setRelation('category', $category);

        return $transaction;
    }

    protected function getRequestMock(string $ip = '1.1.1.1'): Request|m\Mock
    {
        $request = m::mock(Request::class);
        $request->shouldReceive('ip')->andReturn($ip);

        return $request;
    }

    protected function getSut(): HomeContactService
    {
        return new \App\Services\Contacts\Home\HomeContactService(
            m::mock(Contact::class),
            [
                ActiveUserService::class => m::mock(ActiveUserService::class),
                IpService::class => m::mock(IpService::class),
                RetrieveService::class => m::mock(RetrieveService::class),
                UserAccessService::class => m::mock(UserAccessService::class),
            ]
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
