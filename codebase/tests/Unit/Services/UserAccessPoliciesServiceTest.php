<?php

namespace Tests\App\Unit\Services;

use App\Contracts\Models\Contact;
use App\Contracts\Services\Access\UserAccessService;
use App\Contracts\Storage\Access;
use App\Models\Eloquent\Category;
use App\Models\Eloquent\Location;
use App\Models\Eloquent\Transaction;
use App\Services\UserAccessPoliciesService;
use Illuminate\Auth\Access\Gate;
use Illuminate\Container\Container;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Tests\App\Stubs\User;

class UserAccessPoliciesServiceTest extends TestCase
{
    public function testCreateIsAllowed(): void
    {
        $sut = $this->getSut();
        $access = (new \App\Models\Eloquent\Access())->forceFill(['user_id' => 1, 'location_id' => 11]);
        $access->setRelation('location', (new Location())->forceFill(['id' => 11, 'name' => 'Foo']));
        $sut->getUserAccessService()->getProvider()->shouldReceive('getAccessByUserId')
            ->once()
            ->with(1)
            ->andReturn(new Collection([$access]));
        $this->assertTrue($sut->create(new User(1), ['location_id' => 11])->allowed());
    }

    public function testCreateIsNotAllowed(): void
    {
        $sut = $this->getSut();
        $access = (new \App\Models\Eloquent\Access())->forceFill(['user_id' => 1, 'location_id' => 22]);
        $access->setRelation('location', (new Location())->forceFill(['id' => 22, 'name' => 'Foo']));
        $sut->getUserAccessService()->getProvider()->shouldReceive('getAccessByUserId')
            ->once()
            ->with(1)
            ->andReturn(new Collection([$access]));
        $this->assertFalse($sut->create(new User(1), ['location_id' => 11])->allowed());
    }

    public function testCreateWithoutLocationIdIsNotAllowed(): void
    {
        $sut = $this->getSut();
        $sut->getUserAccessService()->getProvider()->shouldReceive('getAccessByUserId')
            ->never();
        $this->assertFalse($sut->create(new User(1), [])->allowed());
    }

    public function testLocationIdIsAllowedContactModel(): void
    {
        $sut = $this->getSut();
        $access = (new \App\Models\Eloquent\Access())->forceFill(['user_id' => 1, 'location_id' => 11]);
        $location = (new Location())->forceFill(['id' => 11, 'name' => 'Foo']);
        $access->setRelation('location', $location);
        $sut->getUserAccessService()->getProvider()->shouldReceive('getAccessByUserId')
            ->once()
            ->with(1)
            ->andReturn(new Collection([$access]));
        $contact = $this->createContact($location);
        $this->assertTrue($sut->update(new User(1), $contact)->allowed());
    }

    public function testLocationIdIsNotAllowedContactModel(): void
    {
        $sut = $this->getSut();
        $access = (new \App\Models\Eloquent\Access())->forceFill(['user_id' => 1, 'location_id' => 11]);
        $location = (new Location())->forceFill(['id' => 11, 'name' => 'Foo']);
        $access->setRelation('location', $location);
        $sut->getUserAccessService()->getProvider()->shouldReceive('getAccessByUserId')
            ->once()
            ->with(1)
            ->andReturn(new Collection([$access]));
        $location2 = (new Location())->forceFill(['id' => 22, 'name' => 'Bar']);
        $contact = $this->createContact($location2);
        $this->assertFalse($sut->update(new User(1), $contact)->allowed());
    }

    protected function createContact(\App\Contracts\Models\Location $location): Contact
    {
        $category = (new Category())->forceFill(['id' => 111, 'location_id' => $location->id]);
        $category->setRelation('location', $location);
        $transaction = (new Transaction())->forceFill(['id' => 1111, 'category_id' => $category->id]);
        $transaction->setRelation('category', $category);
        $contact = (new \App\Models\Eloquent\Contact())->forceFill(['transaction_id' => $transaction->id]);
        $contact->setRelation('transaction', $transaction);

        return $contact;
    }

    protected function getSut(): UserAccessPoliciesService
    {
        $gate = new Gate(Container::getInstance(), function () {
            return new User(1);
        });
        $service = new UserAccessPoliciesService([
            UserAccessService::class => new \App\Services\Access\UserAccessService(m::mock(Access::class)),
        ]);
        $service->setGate($gate);

        return $service;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
