<?php

namespace Tests\App\Unit\Services\Access;

use App\Contracts\Services\Access\UserAccessService;
use App\Contracts\Storage\Access;
use App\Models\Eloquent\Location;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Tests\App\Stubs\User;

class UserAccessServiceTest extends TestCase
{
    public function testFindLocationAllowed(): void
    {
        $sut = $this->getSut();
        $request = $this->getRequest();
        $access = (new \App\Models\Eloquent\Access())->forceFill(['user_id' => 1, 'location_id' => 11]);
        $location = (new Location())->forceFill(['id' => 11, 'name' => 'Foo']);
        $access->setRelation('location', $location);
        $sut->getProvider()->shouldReceive('getAccessByUserId')
            ->once()
            ->with(1)
            ->andReturn(new Collection([$access]));
        $result = $sut->findLocation($request, 11);
        $this->assertSame($location, $result->model);
    }

    public function testFindLocationAllowedCanBeFalseResult(): void
    {
        $sut = $this->getSut();
        $request = $this->getRequest();
        $access = (new \App\Models\Eloquent\Access())->forceFill(['user_id' => 1, 'location_id' => 11]);
        $location = (new Location())->forceFill(['id' => 11, 'name' => 'Foo']);
        $access->setRelation('location', $location);
        $sut->getProvider()->shouldReceive('getAccessByUserId')
            ->once()
            ->with(1)
            ->andReturn(new Collection([$access]));
        $result = $sut->findLocation($request, 22, false);
        $this->assertFalse($result->result);
    }

    public function testFindLocationNotAllowedCanThrowException(): void
    {
        $sut = $this->getSut();
        $request = $this->getRequest();
        $access = (new \App\Models\Eloquent\Access())->forceFill(['user_id' => 1, 'location_id' => 11]);
        $location = (new Location())->forceFill(['id' => 11, 'name' => 'Foo']);
        $access->setRelation('location', $location);
        $sut->getProvider()->shouldReceive('getAccessByUserId')
            ->once()
            ->with(1)
            ->andReturn(new Collection([$access]));
        $this->expectException(AuthorizationException::class);
        $this->expectExceptionMessage('You do not have access to the requested location [22]');
        $result = $sut->findLocation($request, 22);
    }

    public function testGetUserLocations(): void
    {
        $sut = $this->getSut();
        $request = $this->getRequest();
        $access = (new \App\Models\Eloquent\Access())->forceFill(['user_id' => 1, 'location_id' => 11]);
        $location = (new Location())->forceFill(['id' => 11, 'name' => 'Foo']);
        $access->setRelation('location', $location);
        $sut->getProvider()->shouldReceive('getAccessByUserId')
            ->once()
            ->with(1)
            ->andReturn(new Collection([$access]));
        $result = $sut->getUserLocations($request);
        $this->assertSame($location, $result->collection->first());
    }

    protected function getRequest(int $userId = 1): Request
    {
        $request = new Request();
        $request->setUserResolver(function () use ($userId) {
            return new User($userId);
        });

        return $request;
    }

    protected function getSut(): UserAccessService
    {
        return new \App\Services\Access\UserAccessService(m::mock(Access::class));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
