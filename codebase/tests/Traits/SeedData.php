<?php

namespace Tests\App\Traits;

use App\Contracts\Models\Access;
use App\Contracts\Models\Category;
use App\Contracts\Models\Contact;
use App\Contracts\Models\Location;
use App\Contracts\Models\Transaction;
use Smorken\Auth\Proxy\Contracts\Models\User;
use Smorken\Roles\Models\Eloquent\RoleUser;

trait SeedData
{
    protected function addRole(User|\App\Contracts\Models\User $user, int $roleId = 2): void
    {
        RoleUser::factory()->create(['user_id' => $user->id, 'role_id' => $roleId]);
    }

    protected function createAccess(User|\App\Contracts\Models\User $user, Location $location): Access
    {
        return \App\Models\Eloquent\Access::factory()->create(['user_id' => $user->id, 'location_id' => $location->id]);
    }

    protected function createBaseUser(array $attributes = []): \App\Contracts\Models\User
    {
        return \App\Models\Eloquent\User::factory()->create($attributes);
    }

    protected function createCategories(Location $location, int $count, array $attributes = []): array
    {
        $cats = [];
        for ($i = 0; $i < $count; $i++) {
            $cats[] = $this->createCategory($location, $attributes);
        }

        return $cats;
    }

    protected function createCategory(Location $location, array $attributes = []): Category
    {
        $attributes['location_id'] = $location->id;

        return \App\Models\Eloquent\Category::factory()->create($attributes);
    }

    protected function createContact(Transaction $transaction, string $userId): Contact
    {
        return \App\Models\Eloquent\Contact::factory()->create([
            'transaction_id' => $transaction->id, 'user_id' => $userId,
        ]);
    }

    protected function createContacts(Transaction $transaction, string $userId, int $count): array
    {
        $contacts = [];
        for ($i = 0; $i < $count; $i++) {
            $contacts[] = $this->createContact($transaction, $userId);
        }

        return $contacts;
    }

    protected function createLocation(array $attributes = []): Location
    {
        return \App\Models\Eloquent\Location::factory()->create($attributes);
    }

    protected function createTransaction(Category $category, array $attributes = []): Transaction
    {
        $attributes['category_id'] = $category->id;

        return \App\Models\Eloquent\Transaction::factory()->create($attributes);
    }

    protected function createTransactions(Category $category, int $count, array $attributes = []): array
    {
        $transactions = [];
        for ($i = 0; $i < $count; $i++) {
            $transactions[] = $this->createTransaction($category, $attributes);
        }

        return $transactions;
    }

    protected function createUser(array $attributes = []): User
    {
        return \Smorken\Auth\Proxy\Models\Eloquent\User::factory()->create($attributes);
    }
}
