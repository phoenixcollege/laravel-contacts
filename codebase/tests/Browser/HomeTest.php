<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Ip;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Smorken\IpAuth\Models\Eloquent\Active;
use Tests\App\DuskTestCase;
use Tests\App\Traits\SeedData;

class HomeTest extends DuskTestCase
{
    use DatabaseMigrations, SeedData;

    public function testDeleteLastContact(): void
    {
        $this->browse(function (Browser $browser) {
            $user = User::factory()->create(['id' => 1]);
            $location = $this->createLocation();
            $this->createAccess($user, $location);
            $categories = $this->createCategories($location, 3);
            $transactions[$categories[0]->id] = $this->createTransactions($categories[0], 2);
            $transactions[$categories[1]->id] = $this->createTransactions($categories[1], 5);
            $transaction = $this->createTransaction($categories[2]);
            $contact = $this->createContact($transaction, $user->id);
            $this->authIp($location);
            $this->activeUser($user->id);
            $browser->visit('/')
                ->assertSee(sprintf('Last: #%d %s - %s', $contact->id, $transaction->transaction,
                    $categories[2]->category))
                ->clickLink('Delete')
                ->pause(500);
            $last = $browser->text('#last-contact');
            $this->assertEmpty($last);
            $browser->logout();
        });
    }

    public function testFresh(): void
    {
        $this->browse(function (Browser $browser) {
            $user = User::factory()->create(['id' => 1]);
            $location = $this->createLocation();
            $this->createAccess($user, $location);
            $categories = $this->createCategories($location, 3);
            $transactions[$categories[0]->id] = $this->createTransactions($categories[0], 2);
            $transactions[$categories[1]->id] = $this->createTransactions($categories[1], 5);
            $transactions[$categories[2]->id][] = $this->createTransaction($categories[2]);
            $this->authIp($location);
            $this->activeUser($user->id);
            $browser->visit('/');
            /** @var \App\Contracts\Models\Category $category */
            foreach ($categories as $category) {
                $transArray = $transactions[$category->id];
                $browser->with('#category-'.$category->id, function (Browser $catCard) use ($category, $transArray) {
                    $catCard->assertSee($category->category);
                    /** @var \App\Contracts\Models\Transaction $transaction */
                    foreach ($transArray as $transaction) {
                        $catCard->with('#transaction-'.$transaction->id,
                            function (Browser $transBlock) use ($transaction) {
                                $transBlock->assertSeeLink($transaction->transaction);
                            });
                    }
                });
            }
            $last = $browser->text('#last-contact');
            $this->assertEmpty($last);
            $browser->logout();
        });
    }

    public function testFreshWithoutUserWithAccess(): void
    {
        $this->browse(function (Browser $browser) {
            $user = User::factory()->create(['id' => 1]);
            $location = $this->createLocation();
            $categories = $this->createCategories($location, 3);
            $transactions[$categories[0]->id] = $this->createTransactions($categories[0], 2);
            $transactions[$categories[1]->id] = $this->createTransactions($categories[1], 5);
            $transactions[$categories[2]->id][] = $this->createTransaction($categories[2]);
            $this->authIp($location);
            $this->activeUser($user->id);
            $browser->visit('/')
                ->assertSee('You do not have access')
                ->logout();
        });
    }

    public function testIpAuthAndNoSelectedUserRequiresUserSelection(): void
    {
        $this->browse(function (Browser $browser) {
            $location = $this->createLocation();
            $this->authIp($location);
            $browser->visit('/')
                ->assertPathIs('/select')
                ->assertSee('No users found')
                ->logout();
        });
    }

    public function testNewContactFillsLast(): void
    {
        $this->browse(function (Browser $browser) {
            $user = User::factory()->create(['id' => 1]);
            $location = $this->createLocation();
            $this->createAccess($user, $location);
            $categories = $this->createCategories($location, 3);
            $transactions[$categories[0]->id] = $this->createTransactions($categories[0], 2);
            $transactions[$categories[1]->id] = $this->createTransactions($categories[1], 5);
            $transaction = $this->createTransaction($categories[2]);
            $this->authIp($location);
            $this->activeUser($user->id);
            $browser->visit('/')
                ->clickLink($transaction->transaction)
                ->pause(500)
                ->assertSee(sprintf('Last: #1 %s - %s', $transaction->transaction, $categories[2]->category))
                ->logout();
        });
    }

    public function testNoIpAuthAndAllowedUserIsIpAuthSelectionLocationAllowed(): void
    {
        $this->browse(function (Browser $browser) {
            $user = User::factory()->create([
                'id' => 1,
                'username' => 'foobar',
                'email' => 'foobar@example.org',
                'first_name' => 'foo',
                'last_name' => 'bar',
            ]);
            $location = $this->createLocation();
            $this->createAccess($user, $location);
            $browser->loginAs($user)
                ->visit('/')
                ->assertPathIs('/manage/ip-auth')
                ->assertSee($location->name)
                ->logout();
        });
    }

    public function testNoIpAuthAndAllowedUserIsIpAuthSelectionNoLocationsAllowed(): void
    {
        $this->browse(function (Browser $browser) {
            $user = User::factory()->create([
                'id' => 1,
                'username' => 'foobar',
                'email' => 'foobar@example.org',
                'first_name' => 'foo',
                'last_name' => 'bar',
            ]);
            $location = $this->createLocation();
            $browser->loginAs($user)
                ->visit('/')
                ->assertPathIs('/manage/ip-auth')
                ->assertDontSee($location->name)
                ->logout();
        });
    }

    public function testNoIpAuthAndNoUserIsLogin(): void
    {
        $this->browse(function (Browser $browser) {
            $location = $this->createLocation();
            $browser->visit('/')
                ->assertPathIs('/login')
                ->logout();
        });
    }

    public function testSelectUserRedirectsToHome(): void
    {
        $this->browse(function (Browser $browser) {
            $user = User::factory()->create(['id' => 1]);
            $location = $this->createLocation();
            $this->createAccess($user, $location);
            $this->authIp($location);
            $browser->visit('/')->assertPathIs('/select')
                ->assertSee('Please select a user')
                ->assertSee($location->name)
                ->assertSee('Select the active user')
                ->press($user->name())
                ->assertPathIs('/')
                ->assertSee('Change active user ('.$user->name().')')
                ->assertSee('No categories found')
                ->logout();
        });
    }

    protected function activeUser(string|int $user_id): Active
    {
        return Active::factory()->create(['user_id' => $user_id, 'ip' => $this->getIp()]);
    }

    protected function authIp(\App\Contracts\Models\Location $location): Ip
    {
        return Ip::factory()->create(['ip' => $this->getIp(), 'location_id' => $location->id]);
    }

    protected function getIp(): string
    {
        return gethostbyname('selenium');
    }
}
