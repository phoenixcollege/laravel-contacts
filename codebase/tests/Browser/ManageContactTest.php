<?php

namespace Tests\App\Browser;

use Facebook\WebDriver\WebDriverBy;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;
use Tests\App\Traits\SeedData;

class ManageContactTest extends DuskTestCase
{
    use DatabaseMigrations, SeedData;

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUserAndLocationAssignments(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $location = $this->createLocation();
        $this->createAccess($user, $location);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/manage/contact')
                ->assertSee('Contact Management')
                ->assertSee('No records found')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUserAndNoLocationAssignments(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/manage/contact')
                ->assertSee('You do not have any location assignments')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithManageUserAndLocationAssignments(): void
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->addRole($user, 2);
        $location = $this->createLocation();
        $this->createAccess($user, $location);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/manage/contact')
                ->assertSee('Contact Management')
                ->assertSee('No records found')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithManageUserAndNoLocationAssignments(): void
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->addRole($user, 2);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/manage/contact')
                ->assertSee('You do not have any location assignments')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithoutManageUser(): void
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/manage/contact')
                ->assertSee("You don't have permission")
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCanDeleteContact(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $location = $this->createLocation();
        $this->createAccess($user, $location);
        $categories = $this->createCategories($location, 3);
        $t1s = $this->createTransactions($categories[0], 2);
        $t2s = $this->createTransactions($categories[1], 5);
        $t3 = $this->createTransaction($categories[2]);
        $contact = $this->createContact($t1s[0], 'abc123');
        $this->browse(function (Browser $browser) use ($user, $contact) {
            $browser->loginAs($user)
                ->visit('/manage/contact')
                ->with('#row-for-'.$contact->id, function ($t) use ($contact) {
                    $t->assertSee($contact->id)
                        ->clickLink('delete');
                })->assertPathIs('/manage/contact/delete/'.$contact->id)
                ->press('Delete')
                ->assertNotPresent('#row-for-'.$contact->id)
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCanDeleteContactDeletesOneContact(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $location = $this->createLocation();
        $this->createAccess($user, $location);
        $categories = $this->createCategories($location, 3);
        $t1s = $this->createTransactions($categories[0], 2);
        $t2s = $this->createTransactions($categories[1], 5);
        $t3 = $this->createTransaction($categories[2]);
        $contacts = $this->createContacts($t1s[0], 'abc123', 10);
        $this->browse(function (Browser $browser) use ($user, $contacts) {
            $browser->loginAs($user)
                ->visit('/manage/contact');
            $elements = $browser->driver->findElements(WebDriverBy::className('contact'));
            $this->assertCount(10, $elements);
            $browser->with('#row-for-'.$contacts[0]->id, function ($t) {
                $t->clickLink('delete');
            })->press('Delete')
                ->assertPathIs('/manage/contact')
                ->assertNotPresent('#row-for-'.$contacts[0]->id);
            $elements = $browser->driver->findElements(WebDriverBy::className('contact'));
            $this->assertCount(9, $elements);
            $browser->logout();
        });
    }
}
