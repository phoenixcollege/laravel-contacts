<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Category;
use App\Models\Eloquent\Transaction;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminTransactionTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser()
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/transaction')
                ->assertSee('Transaction Administration')
                ->assertSee('No records found');
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser()
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/transaction')
                ->assertSee("You don't have permission");
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCreateNewRecord()
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $cats = Category::factory(3)->create();
            $browser->loginAs($user)
                ->visit('/admin/transaction')
                ->clickLink('New')
                ->assertPathIs('/admin/transaction/create')
                ->type('transaction', 'Test Create')
                ->select('category_id', $cats[0]->id)
                ->check('active')
                ->press('Save')
                ->assertPathIs('/admin/transaction')
                ->assertSee('Test Create');
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCreateNewRecordFailsValidation()
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/transaction')
                ->clickLink('New')
                ->assertPathIs('/admin/transaction/create')
                ->press('Save')
                ->assertPathIs('/admin/transaction/create')
                ->assertSee('transaction field is required')
                ->assertSee('category id field is required');
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testDeleteExistingRecord()
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Transaction::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/transaction')
                ->assertSee($model->transaction)
                ->clickLink('delete')
                ->assertPathIs('/admin/transaction/delete/'.$model->id)
                ->assertSee($model->transaction)
                ->press('Delete')
                ->assertPathIs('/admin/transaction')
                ->assertDontSee($model->transaction);
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testDeleteMissingRecord()
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/transaction/delete/2')
                ->assertSee('The resource you were trying to reach');
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateExistingRecord()
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $cats = Category::factory(3)->create();
            $model = Transaction::factory()->create(['category_id' => $cats[0]->id]);
            $browser->loginAs($user)
                ->visit('/admin/transaction')
                ->assertSee($model->transaction)
                ->clickLink('update')
                ->assertPathIs('/admin/transaction/update/'.$model->id)
                ->assertInputValue('transaction', $model->transaction)
                ->assertSelected('category_id', $cats[0]->id)
                ->assertChecked('active')
                ->type('transaction', 'Test Update')
                ->select('category_id', $cats[1]->id)
                ->press('Save')
                ->assertPathIs('/admin/transaction')
                ->assertSee('Test Update')
                ->assertSee($cats[1]->category);
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateExistingRecordFailsValidation()
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $cats = Category::factory(3)->create();
            $model = Transaction::factory()->create(['category_id' => $cats[0]->id]);
            $browser->loginAs($user)
                ->visit('/admin/transaction')
                ->assertSee($model->transaction)
                ->clickLink('update')
                ->assertPathIs('/admin/transaction/update/'.$model->id)
                ->assertInputValue('transaction', $model->transaction)
                ->assertSelected('category_id', $cats[0]->id)
                ->assertChecked('active')
                ->clear('transaction')
                ->press('Save')
                ->assertPathIs('/admin/transaction/update/'.$model->id)
                ->assertSee('transaction field is required');
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateMissingRecord()
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/transaction/update/2')
                ->assertSee('The resource you were trying to reach');
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testViewExistingRecord()
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $cats = Category::factory(3)->create();
            $model = Transaction::factory()->create(['category_id' => $cats[0]->id]);
            $browser->loginAs($user)
                ->visit('/admin/transaction')
                ->assertSee($model->transaction)
                ->clickLink('1')
                ->assertPathIs('/admin/transaction/view/'.$model->id)
                ->assertSee($model->transaction);
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testViewMissingRecord()
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/transaction/view/2')
                ->assertSee('The resource you were trying to reach');
        });
    }
}
