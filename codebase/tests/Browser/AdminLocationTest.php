<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Location;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;

class AdminLocationTest extends DuskTestCase
{
    use DatabaseMigrations;

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/location')
                ->assertSee('Location Administration')
                ->assertSee('No records found')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/location')
                ->assertSee("You don't have permission")
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCreateNewRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/location')
                ->clickLink('New')
                ->assertPathIs('/admin/location/create')
                ->type('name', 'Test Create')
                ->check('active')
                ->press('Save')
                ->assertPathIs('/admin/location')
                ->assertSee('Test Create')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCreateNewRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/location')
                ->clickLink('New')
                ->assertPathIs('/admin/location/create')
                ->press('Save')
                ->assertPathIs('/admin/location/create')
                ->assertSee('name field is required')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testDeleteExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Location::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/location')
                ->assertSee($model->name)
                ->clickLink('delete')
                ->assertPathIs('/admin/location/delete/'.$model->id)
                ->assertSee($model->name)
                ->press('Delete')
                ->assertPathIs('/admin/location')
                ->assertDontSee($model->name)
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testDeleteMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/location/delete/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Location::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/location')
                ->assertSee($model->name)
                ->clickLink('update')
                ->assertPathIs('/admin/location/update/'.$model->id)
                ->assertInputValue('name', $model->name)
                ->assertChecked('active')
                ->type('name', 'Test Update')
                ->press('Save')
                ->assertPathIs('/admin/location')
                ->assertSee('Test Update')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateExistingRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Location::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/location')
                ->assertSee($model->name)
                ->clickLink('update')
                ->assertPathIs('/admin/location/update/'.$model->id)
                ->assertInputValue('name', $model->name)
                ->assertChecked('active')
                ->clear('name')
                ->press('Save')
                ->assertPathIs('/admin/location/update/'.$model->id)
                ->assertSee('name field is required')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/location/update/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testViewExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Location::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/location')
                ->assertSee($model->name)
                ->clickLink('1')
                ->assertPathIs('/admin/location/view/'.$model->id)
                ->assertSee($model->name)
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testViewMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/location/view/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }
}
