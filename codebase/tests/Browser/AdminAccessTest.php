<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Access;
use App\Models\Eloquent\Location;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;
use Tests\App\Traits\SeedData;

class AdminAccessTest extends DuskTestCase
{
    use DatabaseMigrations, SeedData;

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testAddAccessToUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $u = User::factory()->create();
            $locs = Location::factory(2)->create();
            $key = '#location-%d-1';
            $browser->loginAs($user)
                ->visit('/admin/access')
                ->with('#row-for-'.$u->id, function (Browser $t) use ($u) {
                    $t->assertSee('--')
                        ->clickLink($u->id);
                })
                ->assertPathIs('/admin/access/assign/'.$u->id)
                ->assertNotChecked(sprintf($key, $locs[0]->id))
                ->assertNotChecked(sprintf($key, $locs[1]->id))
                ->check(sprintf($key, $locs[0]->id))
                ->press('Save')
                ->assertPathIs('/admin/access')
                ->with('#row-for-'.$u->id, function (Browser $t) use ($locs) {
                    $t->assertSee($locs[0]->name)
                        ->assertDontSee($locs[1]->name);
                })
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/access')
                ->assertSee('Access Administration')
                ->with('#row-for-'.$user->id, function (Browser $row) {
                    $row->assertSee('foo')
                        ->assertSee('bar');
                })
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 99,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/access')
                ->assertSee("You don't have permission")
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testRemoveAccessFromUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $u = User::factory()->create();
            $locs = Location::factory(2)->create();
            $access = Access::factory()->create(['user_id' => $u->id, 'location_id' => $locs[0]->id]);
            $key = '#location-%d-1';
            $browser->loginAs($user)
                ->visit('/admin/access')
                ->with('#row-for-'.$u->id, function (Browser $t) use ($u, $locs) {
                    $t->assertSee($locs[0]->name)
                        ->assertDontSee($locs[1]->name)
                        ->clickLink($u->id);
                })
                ->assertPathIs('/admin/access/assign/'.$u->id)
                ->assertChecked(sprintf($key, $locs[0]->id))
                ->assertNotChecked(sprintf($key, $locs[1]->id))
                ->uncheck(sprintf($key, $locs[0]->id))
                ->press('Save')
                ->assertPathIs('/admin/access')
                ->with('#row-for-'.$u->id, function (Browser $t) {
                    $t->assertSee('--');
                })
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testRemoveAllAccessFromUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $u = User::factory()->create();
            $locs = Location::factory(2)->create();
            $access = Access::factory()->create(['user_id' => $u->id, 'location_id' => $locs[0]->id]);
            $browser->loginAs($user)
                ->visit('/admin/access')
                ->with('#row-for-'.$u->id, function (Browser $t) use ($locs) {
                    $t->assertSee($locs[0]->name)
                        ->assertDontSee($locs[1]->name)
                        ->clickLink('remove all');
                })
                ->assertPathIs('/admin/access')
                ->with('#row-for-'.$u->id, function (Browser $t) {
                    $t->assertSee('--');
                })
                ->logout();
        });
    }
}
