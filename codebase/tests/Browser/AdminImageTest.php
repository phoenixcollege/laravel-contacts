<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Image;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;
use Tests\App\Traits\SeedData;

class AdminImageTest extends DuskTestCase
{
    use DatabaseMigrations, SeedData;

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/image')
                ->assertSee('Image Administration')
                ->assertSee('No records found')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/image')
                ->assertSee("You don't have permission")
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCreateNewRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/image')
                ->clickLink('New')
                ->assertPathIs('/admin/image/create')
                ->attach('image', __DIR__.'/logo.png')
                ->press('Save')
                ->assertPathIs('/admin/image')
                ->assertSee('logo')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCreateNewRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/image')
                ->clickLink('New')
                ->assertPathIs('/admin/image/create')
                ->press('Save')
                ->assertPathIs('/admin/image/create')
                ->assertSee('image field is required')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCreateNewRecordOverrideName(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/image')
                ->clickLink('New')
                ->assertPathIs('/admin/image/create')
                ->attach('image', __DIR__.'/logo.png')
                ->type('name', 'Create Image')
                ->press('Save')
                ->assertPathIs('/admin/image')
                ->assertSee('Create Image')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testDeleteExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Image::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/image')
                ->assertSee($model->name)
                ->clickLink('delete')
                ->assertPathIs('/admin/image/delete/'.$model->id)
                ->assertSee($model->name)
                ->press('Delete')
                ->assertPathIs('/admin/image')
                ->assertDontSee($model->name)
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testDeleteMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/image/delete/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Image::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/image')
                ->assertSee($model->name)
                ->with('#row-for-'.$model->id, function (Browser $row) {
                    $row->clickLink('update');
                })
                ->assertPathIs('/admin/image/update/'.$model->id)
                ->assertInputValue('name', $model->name)
                ->assertMissing('image')
                ->type('name', 'Test Update')
                ->press('Save')
                ->assertPathIs('/admin/image')
                ->assertSee('Test Update')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateExistingRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Image::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/image')
                ->assertSee($model->name)
                ->clickLink('update')
                ->assertPathIs('/admin/image/update/'.$model->id)
                ->assertInputValue('name', $model->name)
                ->clear('name')
                ->press('Save')
                ->assertPathIs('/admin/image/update/'.$model->id)
                ->assertSee('name field is required')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/image/update/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }
}
