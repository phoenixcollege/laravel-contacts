<?php

namespace Tests\App\Browser;

use App\Models\Eloquent\Category;
use App\Models\Eloquent\Location;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\DuskTestCase;
use Tests\App\Traits\SeedData;

class AdminCategoryTest extends DuskTestCase
{
    use DatabaseMigrations, SeedData;

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee('Category Administration')
                ->assertSee('No records found')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testBaseRouteWithoutAdminUser(): void
    {
        $user = User::factory()->create([
            'id' => 2,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee("You don't have permission")
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCreateNewRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $location = Location::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->clickLink('New')
                ->assertPathIs('/admin/category/create')
                ->type('category', 'Test Create')
                ->select('location_id', $location->id)
                ->check('active')
                ->press('Save')
                ->assertPathIs('/admin/category')
                ->assertSee('Test Create')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testCreateNewRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->clickLink('New')
                ->assertPathIs('/admin/category/create')
                ->press('Save')
                ->assertPathIs('/admin/category/create')
                ->assertSee('category field is required')
                ->assertSee('location id field is required')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testDeleteExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Category::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee($model->category)
                ->clickLink('delete')
                ->assertPathIs('/admin/category/delete/'.$model->id)
                ->assertSee($model->category)
                ->press('Delete')
                ->assertPathIs('/admin/category')
                ->assertDontSee($model->category)
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testDeleteMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category/delete/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $loc = Location::factory()->create();
            $model = Category::factory()->create(['location_id' => $loc->id]);
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee($model->category)
                ->clickLink('update')
                ->assertPathIs('/admin/category/update/'.$model->id)
                ->assertInputValue('category', $model->category)
                ->assertSelected('location_id', $loc->id)
                ->assertChecked('active')
                ->type('category', 'Test Update')
                ->press('Save')
                ->assertPathIs('/admin/category')
                ->assertSee('Test Update')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateExistingRecordFailsValidation(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $loc = Location::factory()->create();
            $model = Category::factory()->create(['location_id' => $loc->id]);
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee($model->category)
                ->clickLink('update')
                ->assertPathIs('/admin/category/update/'.$model->id)
                ->assertInputValue('category', $model->category)
                ->assertSelected('location_id', $loc->id)
                ->assertChecked('active')
                ->clear('category')
                ->press('Save')
                ->assertPathIs('/admin/category/update/'.$model->id)
                ->assertSee('category field is required')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testUpdateMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $model = Category::factory()->create();
            $browser->loginAs($user)
                ->visit('/admin/category/update/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testViewExistingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $loc = Location::factory()->create();
            $model = Category::factory()->create(['location_id' => $loc->id]);
            $browser->loginAs($user)
                ->visit('/admin/category')
                ->assertSee($model->category)
                ->clickLink('1')
                ->assertPathIs('/admin/category/view/'.$model->id)
                ->assertSee($model->category)
                ->logout();
        });
    }

    /**
     * @test
     *
     * @throws \Throwable
     */
    public function testViewMissingRecord(): void
    {
        $user = User::factory()->create([
            'id' => 1,
            'username' => 'foobar',
            'email' => 'foobar@example.org',
            'first_name' => 'foo',
            'last_name' => 'bar',
        ]);
        $this->browse(function (Browser $browser) use ($user) {
            $browser->loginAs($user)
                ->visit('/admin/category/view/2')
                ->assertSee('The resource you were trying to reach')
                ->logout();
        });
    }
}
