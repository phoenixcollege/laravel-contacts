<?php

namespace Tests\App\Feature\Access;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Smorken\Roles\Contracts\Role;
use Smorken\Roles\Support\DefineGates;
use Tests\App\BrowserKitTestCase;
use Tests\App\Traits\SeedData;

class ManageContactsTest extends BrowserKitTestCase
{
    use DatabaseMigrations, SeedData;

    public function testManagerAndAccess(): void
    {
        $location = $this->createLocation();
        $user = $this->createUser();
        $this->addRole($user);
        $this->createAccess($user, $location);
        $this->actingAs($user)
            ->get('/manage/contact')
            ->assertResponseStatus(200)
            ->see('No records found')
            ->see($location->name);
    }

    public function testManagerAndAccessCanDeleteContact(): void
    {
        $location = $this->createLocation();
        $user = $this->createUser();
        $this->addRole($user);
        $this->createAccess($user, $location);
        $category = $this->createCategory($location);
        $transaction = $this->createTransaction($category);
        $user2 = $this->createBaseUser();
        $contact = $this->createContact($transaction, $user2->id);
        $this->actingAs($user)
            ->get('/manage/contact')
            ->seeElement('#row-for-'.$contact->id)
            ->delete('/manage/contact/delete/'.$contact->id)
            ->followRedirects()
            ->seePageIs('/manage/contact?f_locationId='.$location->id)
            ->dontSeeElement('#row-for-'.$contact->id);
    }

    public function testManagerAndAccessCanSeeContact(): void
    {
        $location = $this->createLocation();
        $user = $this->createUser();
        $this->addRole($user);
        $this->createAccess($user, $location);
        $category = $this->createCategory($location);
        $transaction = $this->createTransaction($category);
        $user2 = $this->createBaseUser();
        $contact = $this->createContact($transaction, $user2->id);
        $this->actingAs($user)
            ->get('/manage/contact')
            ->assertResponseStatus(200)
            ->dontSee('No records found')
            ->seeLink('delete')
            ->seeElement('#row-for-'.$contact->id);
    }

    public function testManagerAndNoAccess(): void
    {
        $location = $this->createLocation(['name' => 'Foo Location of Foo']);
        $user = $this->createUser();
        $this->addRole($user);
        $this->actingAs($user)
            ->get('/manage/contact')
            ->assertResponseStatus(403);
    }

    public function testManagerAndNoAccessCannotDeleteContact(): void
    {
        $location = $this->createLocation();
        $user = $this->createUser();
        $this->addRole($user);
        $category = $this->createCategory($location);
        $transaction = $this->createTransaction($category);
        $user2 = $this->createBaseUser();
        $contact = $this->createContact($transaction, $user2->id);
        $this->actingAs($user)
            ->delete('/manage/contact/delete/'.$contact->id)
            ->assertResponseStatus(403);
    }

    public function testManagerAndNoAccessCannotSeeContact(): void
    {
        $location = $this->createLocation();
        $user = $this->createUser();
        $this->addRole($user);
        $category = $this->createCategory($location);
        $transaction = $this->createTransaction($category);
        $user2 = $this->createBaseUser();
        $contact = $this->createContact($transaction, $user2->id);
        $this->actingAs($user)
            ->get('/manage/contact')
            ->assertResponseStatus(403);
    }

    public function testNotManagerAndNoAccess(): void
    {
        $location = $this->createLocation(['name' => 'Foo Location of Foo']);
        $user = $this->createUser();
        $this->actingAs($user)
            ->get('/manage/contact')
            ->assertResponseStatus(403)
            ->dontSee($location->name);
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->seedTables();
        $this->defineGates();
    }

    protected function tearDown(): void
    {
        /** @var Role $r */
        $r = $this->app[Role::class];
        $r->resetUser();
        DefineGates::reset();
        parent::tearDown();
    }
}
