<?php

namespace Tests\App\Feature\Report;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Smorken\Auth\Proxy\Models\Eloquent\User;
use Tests\App\TestCase;
use Tests\App\Traits\SeedData;

class AdminReportTest extends TestCase
{
    use DatabaseMigrations, SeedData, WithoutMiddleware;

    public function testExport(): void
    {
        $user = User::factory()->create(['id' => 1]);
        $location = $this->createLocation();
        $this->createAccess($user, $location);
        $categories = $this->createCategories($location, 3);
        $transactions[$categories[0]->id] = $this->createTransactions($categories[0], 2);
        $transactions[$categories[1]->id] = $this->createTransactions($categories[1], 5);
        $transaction = $this->createTransaction($categories[2]);
        $contact = $this->createContact($transaction, $user->id);
        $response = $this->get('/admin/report/export');
        $response->assertStatus(200);
        $lines = explode(PHP_EOL, $response->getContent());
        $this->assertEquals('id,user_id,user_name,transaction_id,transaction,category_id,category,location_id,location,created,updated',
            $lines[0]);
        $this->assertStringStartsWith(
            sprintf(
                '%d,%d,"%s",%d,"%s",%d,"%s",%d,"%s",',
                $contact->id,
                $contact->user_id,
                $user->name(),
                $contact->transaction_id,
                $transaction->transaction,
                $transaction->category_id,
                $categories[2]->category,
                $categories[2]->location_id,
                $location->name
            ),
            $lines[1]
        );
    }
}
