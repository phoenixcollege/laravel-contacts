<?php

return [
    'height' => 100,
    'width' => 100,
    'option' => 'auto',
    'output' => 'png',
    'base64' => false,
];
