<?php

return [
    'concrete' => [
        \App\Storage\Eloquent\Access::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Access::class,
            ],
        ],
        \App\Storage\Eloquent\Category::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Category::class,
            ],
            'cacheOptions' => [
                'forgetAuto' => [['active']],
            ],
        ],
        \App\Storage\Eloquent\Contact::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Contact::class,
            ],
        ],
        \App\Storage\Eloquent\Image::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Image::class,
            ],
        ],
        \App\Storage\Eloquent\Location::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Location::class,
            ],
            'cacheOptions' => [
                'forgetAuto' => [['active']],
            ],
        ],
        \App\Storage\Eloquent\Transaction::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\Transaction::class,
            ],
        ],
        \App\Storage\Eloquent\User::class => [
            'model' => [
                'impl' => \App\Models\Eloquent\User::class,
            ],
        ],
    ],
    'contract' => [
        \App\Contracts\Storage\Access::class => \App\Storage\Eloquent\Access::class,
        \App\Contracts\Storage\Category::class => \App\Storage\Eloquent\Category::class,
        \App\Contracts\Storage\Contact::class => \App\Storage\Eloquent\Contact::class,
        \App\Contracts\Storage\Image::class => \App\Storage\Eloquent\Image::class,
        \App\Contracts\Storage\Location::class => \App\Storage\Eloquent\Location::class,
        \App\Contracts\Storage\Transaction::class => \App\Storage\Eloquent\Transaction::class,
        \App\Contracts\Storage\User::class => \App\Storage\Eloquent\User::class,
    ],
];
