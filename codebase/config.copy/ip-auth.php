<?php

return [
    'routes' => [
        'load' => env('IPAUTH_LOAD_ROUTES', true),
        'user_select' => [
            'middleware' => ['web', \Smorken\IpAuth\Http\Middleware\IpActive::class],
            'prefix' => null,
            'controller' => \Smorken\IpAuth\Http\Controllers\UserController::class,
            'redirect' => [\App\Http\Controllers\HomeController::class, 'index'],
            'role' => env('IPAUTH_USER_ROLE', 100), // Role "level" for user
        ],
        'authorize' => [
            'middleware' => ['web', 'auth', 'can:role-manage'],
            'prefix' => 'manage',
            'controller' => \Smorken\IpAuth\Http\Controllers\AuthorizeController::class,
            'redirect' => [\App\Http\Controllers\HomeController::class, 'index'],
        ],
    ],
    'masks' => [

    ],
    'times' => [
        'default' => env('IPAUTH_DEFAULT', '+7 days'),
        'data' => [
            '+1 hour' => '1 hour',
            '+2 hours' => '2 hours',
            '+4 hours' => '4 hours',
            '+8 hours' => '8 hours',
            '+12 hours' => '12 hours',
            '+16 hours' => '16 hours',
            '+1 day' => '1 day',
            '+2 days' => '2 days',
            '+5 days' => '5 days',
            '+7 days' => '7 days',
        ],
    ],
    'check' => [
        'provider' => \App\IpAuth\Checks\LocationAccess::class,
        'params' => [
            'handler' => \App\Contracts\Services\Access\UserAccessService::class,
            'ipService' => \Smorken\IpAuth\Contracts\Services\Ip\ValidIpService::class,
        ],
    ],
    'services' => [
        \Smorken\IpAuth\Contracts\Services\Authorize\AuthorizeService::class => \Smorken\IpAuth\Services\Authorize\AuthorizeService::class,
        \Smorken\IpAuth\Contracts\Services\Ip\ValidIpService::class => \Smorken\IpAuth\Services\Ip\ValidIpService::class,
        \Smorken\IpAuth\Contracts\Services\User\ActiveUserService::class => \Smorken\IpAuth\Services\User\ActiveUserService::class,
        \Smorken\IpAuth\Contracts\Services\User\SelectService::class => \App\IpAuth\Services\User\SelectService::class,
    ],
    'providers' => [
        'concrete' => [
            \App\Storage\Eloquent\Ip::class => [
                'model' => [
                    'impl' => \App\Models\Eloquent\Ip::class,
                ],
            ],
            \App\Storage\Eloquent\User::class => [
                'model' => [
                    'impl' => \App\Models\Eloquent\User::class,
                ],
            ],
            \Smorken\IpAuth\Storage\Eloquent\Active::class => [
                'model' => [
                    'impl' => \Smorken\IpAuth\Models\Eloquent\Active::class,
                ],
            ],
            \Smorken\IpAuth\Storage\Eloquent\Ip::class => [
                'model' => [
                    'impl' => \Smorken\IpAuth\Models\Eloquent\Ip::class,
                ],
            ],
            \Smorken\IpAuth\Storage\Eloquent\User::class => [
                'model' => [
                    'impl' => \Smorken\IpAuth\Models\Eloquent\User\Role::class,
                ],
            ],
        ],
        'contract' => [
            \Smorken\IpAuth\Contracts\Storage\Active::class => \Smorken\IpAuth\Storage\Eloquent\Active::class,
            \Smorken\IpAuth\Contracts\Storage\Ip::class => \App\Storage\Eloquent\Ip::class,
            \Smorken\IpAuth\Contracts\Storage\User::class => \App\Storage\Eloquent\User::class,
        ],
    ],
];
