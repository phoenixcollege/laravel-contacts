<?php

return [
    'autoload' => env('SERVICES_AUTOLOAD', false),
    'path' => env('SERVICES_INVOKABLE_PATH', 'Providers/Services'),
    'invokables' => [
        \App\Providers\Services\Admin\CategoryServices::class,
        \App\Providers\Services\Admin\ContactServices::class,
        \App\Providers\Services\Admin\ImageServices::class,
        \App\Providers\Services\Admin\LocationServices::class,
        \App\Providers\Services\Admin\TransactionServices::class,
        \App\Providers\Services\Admin\UserServices::class,
        \App\Providers\Services\Home\CategoryServices::class,
        \App\Providers\Services\Home\ContactServices::class,
        \App\Providers\Services\Manage\ContactServices::class,
        \App\Providers\Services\AccessServices::class,
        \App\Providers\Services\CategoryServices::class,
        \App\Providers\Services\ImageServices::class,
        \App\Providers\Services\LocationServices::class,
        \App\Providers\Services\TransactionServices::class,
    ],
];
