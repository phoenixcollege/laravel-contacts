<?php

use Smorken\Auth\Proxy\Http\Controllers\Admin\User\Controller;

return [
    'guest' => [],
    'auth' => [],

    'role-manage' => [
        [
            'name' => 'Authorize',
            'action' => [\Smorken\IpAuth\Http\Controllers\AuthorizeController::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Contacts',
            'action' => [\App\Http\Controllers\Manage\Contact\Controller::class, 'index'],
            'children' => [],
        ],
    ],
    'role-admin' => [
        [
            'name' => 'Pin Users',
            'action' => [\Smorken\PinAuth\Http\Controllers\Admin\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Access',
            'action' => [\App\Http\Controllers\Admin\Access\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Report',
            'action' => [\App\Http\Controllers\Admin\Report\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Transactions',
            'action' => [\App\Http\Controllers\Admin\Transaction\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Categories',
            'action' => [\App\Http\Controllers\Admin\Category\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Images',
            'action' => [\App\Http\Controllers\Admin\Image\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Locations',
            'action' => [\App\Http\Controllers\Admin\Location\Controller::class, 'index'],
            'children' => [],
        ],
        [
            'name' => 'Users',
            'action' => [Controller::class, 'index'],
            'children' => [],
        ],
    ],
];
