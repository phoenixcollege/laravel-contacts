<?php

use Illuminate\Support\Facades\Route;

//Route::get('/errorz', [\App\Http\Controllers\Api\ProbeController::class, 'errorz']);
Route::get('/healthz', [\App\Http\Controllers\Api\ProbeController::class, 'healthz']);
Route::get('/readyz', [\App\Http\Controllers\Api\ProbeController::class, 'readyz']);
