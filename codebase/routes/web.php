<?php

use Illuminate\Support\Facades\Route;

Route::get('/image/{id}', \App\Http\Controllers\ImageController::class)->name('view.image');

Route::middleware(['ip-active', 'user-active'])
    ->group(function () {
        Route::get('/', [\App\Http\Controllers\HomeController::class, 'index']);
        Route::get('/add/{transactionId}', [\App\Http\Controllers\HomeController::class, 'add']);
        Route::get('/delete-last', [\App\Http\Controllers\HomeController::class, 'deleteLast']);
    });

Route::middleware(['auth', 'can:role-manage'])
    ->prefix('manage')
    ->group(function () {
        Route::prefix('contact')
            ->group(function () {
                $controller = \App\Http\Controllers\Manage\Contact\Controller::class;
                Route::get('/', [$controller, 'index']);
                Route::get('/delete/{id}', [$controller, 'delete']);
                Route::delete('/delete/{id}', [$controller, 'doDelete']);
            });
    });

Route::middleware(['auth', 'can:role-admin'])
    ->prefix('admin')
    ->group(function () {
        Route::prefix('access')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\Admin\Access\Controller::class, 'index']);
                Route::get('/assign/{id}', [\App\Http\Controllers\Admin\Access\Controller::class, 'assign']);
                Route::post('/assign/{id}', [\App\Http\Controllers\Admin\Access\Controller::class, 'doAssign']);
                Route::get('/remove/{id}', [\App\Http\Controllers\Admin\Access\Controller::class, 'remove']);
            });
        Route::prefix('category')
            ->group(function () {
                \Smorken\Support\Routes::create(\App\Http\Controllers\Admin\Category\Controller::class);
            });
        Route::prefix('image')
            ->group(function () {
                \Smorken\Support\Routes::create(\App\Http\Controllers\Admin\Image\Controller::class);
            });
        Route::prefix('location')
            ->group(function () {
                \Smorken\Support\Routes::create(\App\Http\Controllers\Admin\Location\Controller::class);
            });
        Route::prefix('pin-user')
            ->group(function () {
                \Smorken\Support\Routes::create(\Smorken\PinAuth\Http\Controllers\Admin\Controller::class);
            });
        Route::prefix('report')
            ->group(function () {
                Route::get('/', [\App\Http\Controllers\Admin\Report\Controller::class, 'index']);
                Route::get('/export', [\App\Http\Controllers\Admin\Report\Controller::class, 'export']);
            });
        Route::prefix('transaction')
            ->group(function () {
                \Smorken\Support\Routes::create(\App\Http\Controllers\Admin\Transaction\Controller::class);
            });
    });
