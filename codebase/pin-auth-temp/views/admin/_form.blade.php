@include('_preset.input.g_input', ['name' => 'name', 'title' => 'Name'])
@include('_preset.input.g_input', ['type' => 'password', 'name' => 'pin', 'title' => 'Pin', 'value' => '', 'autocomplete' => 'off'])
@include('_preset.input.g_input', ['type' => 'password', 'name' => 'pin_confirmation', 'title' => 'Confirm Pin', 'autocomplete' => 'off'])

