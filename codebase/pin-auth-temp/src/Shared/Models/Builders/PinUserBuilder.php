<?php
declare(strict_types=1);

namespace Smorken\PinAuth\Shared\Models\Builders;

use Illuminate\Contracts\Database\Eloquent\Builder as EloquentBuilder;
use Smorken\Model\Filters\FilterHandler;
use Smorken\Model\QueryBuilders\Builder;
use Smorken\QueryStringFilter\Concerns\WithQueryStringFilter;

class PinUserBuilder extends Builder
{
    use WithQueryStringFilter;

    public function defaultOrder(): EloquentBuilder
    {
        return $this->orderBy('name');
    }

    public function nameLike(string $name): EloquentBuilder
    {
        return $this->where('name', 'LIKE', '%'.$name.'%');
    }

    public function pinIs(string $hash): EloquentBuilder
    {
        return $this->where('pin', '=', $hash);
    }

    protected function getFilterHandlersForFilters(): array
    {
        return [
            new FilterHandler('name', 'nameLike'),
        ];
    }
}
