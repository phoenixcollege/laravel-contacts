<?php
declare(strict_types=1);

namespace Smorken\PinAuth\Shared\Validation\RuleProviders;

class PinUserRules
{

    public static function rules(array $overrides = []): array
    {
        return [
            'name' => 'required|max:64',
            'pin' => 'required|max:64|confirmed',
            ...$overrides
        ];
    }
}
