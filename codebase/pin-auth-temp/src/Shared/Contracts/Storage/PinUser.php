<?php
declare(strict_types=1);

namespace Smorken\PinAuth\Shared\Contracts\Storage;

use Smorken\Storage\Contracts\Base;

interface PinUser extends Base
{

}
