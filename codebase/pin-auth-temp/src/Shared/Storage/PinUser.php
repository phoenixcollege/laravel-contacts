<?php
declare(strict_types=1);

namespace Smorken\PinAuth\Shared\Storage;

use Smorken\PinAuth\Shared\Validation\RuleProviders\PinUserRules;
use Smorken\Storage\Eloquent;

class PinUser extends Eloquent implements \Smorken\PinAuth\Shared\Contracts\Storage\PinUser
{

    public function validationRules(array $override = []): array
    {
        return PinUserRules::rules($override);
    }
}
