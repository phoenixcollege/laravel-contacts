<?php
declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Services;

use Smorken\Service\Services\CrudByStorageProviderServices;

class CrudServices extends CrudByStorageProviderServices implements
    \Smorken\PinAuth\Admin\Contracts\Services\CrudServices
{

}
