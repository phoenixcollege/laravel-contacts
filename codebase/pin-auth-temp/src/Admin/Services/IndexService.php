<?php
declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Services;

use Smorken\Service\Services\PaginateByFilterService;

class IndexService extends PaginateByFilterService implements \Smorken\PinAuth\Admin\Contracts\Services\IndexService
{

}
