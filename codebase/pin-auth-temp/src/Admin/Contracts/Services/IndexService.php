<?php
declare(strict_types=1);

namespace Smorken\PinAuth\Admin\Contracts\Services;

interface IndexService extends \Smorken\Service\Contracts\Services\IndexService
{

}
