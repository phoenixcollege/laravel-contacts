<?php

declare(strict_types=1);

namespace Smorken\PinAuth\Http\Controllers\Admin;

use Smorken\Controller\View\WithService\CrudController;
use Smorken\PinAuth\Admin\Contracts\Services\CrudServices;
use Smorken\PinAuth\Admin\Contracts\Services\IndexService;

class Controller extends CrudController
{
    protected string $baseView = 'sm-pin-auth::admin';

    public function __construct(CrudServices $crudServices, IndexService $indexService)
    {
        parent::__construct($crudServices, $indexService);
    }
}
