<?php

declare(strict_types=1);

namespace Smorken\PinAuth;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Validation\Factory;
use Smorken\PinAuth\Admin\Contracts\Services\CrudServices;
use Smorken\PinAuth\Admin\Contracts\Services\IndexService;
use Smorken\PinAuth\Shared\Contracts\Hasher;
use Smorken\PinAuth\Shared\Contracts\Models\PinUser;
use Smorken\PinAuth\Shared\Hashers\Sha256Hasher;
use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Service\Contracts\Services\ValidatorService;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->loadViews();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        \Smorken\PinAuth\Shared\Models\Eloquent\PinUser::setHasherResolver(fn() => $this->app[Hasher::class]);
    }

    public function register(): void
    {
        $this->registerHasher();
        $this->registerModel();
        $this->registerStorageProvider();
        $this->registerServices();
    }

    protected function loadViews(): void
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'sm-pin-auth');
    }

    protected function registerHasher(): void
    {
        $this->app->scoped(Hasher::class, function (Application $app) {
            $salt = env('PINAUTH_HASHER_SALT');
            $key = $app['config']->get('app.key');

            return new Sha256Hasher($salt, $key);
        });
    }

    protected function registerModel(): void
    {
        $this->app->scoped(PinUser::class, function (Application $app) {
            return new \Smorken\PinAuth\Shared\Models\Eloquent\PinUser();
        });
    }

    protected function registerServices(): void
    {
        $this->app->scoped(IndexService::class, function (Application $app) {
            return new \Smorken\PinAuth\Admin\Services\IndexService(
                $app[\Smorken\PinAuth\Shared\Contracts\Storage\PinUser::class],
                [
                    FilterService::class => new \Smorken\Service\Services\FilterService(),
                ]
            );
        });
        $this->app->scoped(CrudServices::class, function (Application $app) {
            $provider = $app[\Smorken\PinAuth\Shared\Contracts\Storage\PinUser::class];
            $services = [
                ValidatorService::class => new \Smorken\Service\Services\ValidatorService($app[Factory::class]),
                FilterService::class => new \Smorken\Service\Services\FilterService(),
            ];

            return \Smorken\PinAuth\Admin\Services\CrudServices::createByStorageProvider($provider, $services);
        });
    }

    protected function registerStorageProvider(): void
    {
        $this->app->scoped(\Smorken\PinAuth\Shared\Contracts\Storage\PinUser::class, function (Application $app) {
            return new \Smorken\PinAuth\Shared\Storage\PinUser($app[PinUser::class]);
        });
    }
}
